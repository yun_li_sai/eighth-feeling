package cn.ef.repository;

import cn.ef.pojo.detail.Detail;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;


@Component
public interface DetailRepository extends ElasticsearchRepository<Detail,Long> {

}
