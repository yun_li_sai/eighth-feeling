package cn.ef.service;

import cn.ef.pojo.detail.Game_grade;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.Game_gradeMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGame_gradeService {

     @Resource
     private Game_gradeMapper game_gradeMapper;

     @RequestMapping(value = "/getGame_gradeById",method = RequestMethod.POST)
     public Game_grade getGame_gradeById(@RequestParam("id") Long id)throws Exception{
        return game_gradeMapper.getGame_gradeById(id);
     }

     @RequestMapping(value = "/getGame_gradeListByMap",method = RequestMethod.POST)
     public List<Game_grade>	getGame_gradeListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_gradeMapper.getGame_gradeListByMap(param);
     }

     @RequestMapping(value = "/getGame_gradeCountByMap",method = RequestMethod.POST)
     public Integer getGame_gradeCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_gradeMapper.getGame_gradeCountByMap(param);
     }

     @RequestMapping(value = "/addGame_grade",method = RequestMethod.POST)
     public Integer addGame_grade(@RequestBody Game_grade game_grade)throws Exception{
        game_grade.setA1(new Date().toString());
        return game_gradeMapper.insertGame_grade(game_grade);
     }

     @RequestMapping(value = "/modifyGame_grade",method = RequestMethod.POST)
     public Integer modifyGame_grade(@RequestBody Game_grade game_grade)throws Exception{
        game_grade.setA2(new Date().toString());
        return game_gradeMapper.updateGame_grade(game_grade);
     }
}
