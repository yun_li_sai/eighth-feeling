package cn.ef.service;

import cn.ef.pojo.detail.Gamerelation;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.GamerelationMapper;



import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGamerelationService {

     @Resource
     private GamerelationMapper gamerelationMapper;

     @RequestMapping(value = "/getGamerelationById",method = RequestMethod.POST)
     public Gamerelation getGamerelationById(@RequestParam("id") Long id)throws Exception{
        return gamerelationMapper.getGamerelationById(id);
     }

     @RequestMapping(value = "/getGamerelationListByMap",method = RequestMethod.POST)
     public List<Gamerelation>	getGamerelationListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return gamerelationMapper.getGamerelationListByMap(param);
     }

     @RequestMapping(value = "/getGamerelationCountByMap",method = RequestMethod.POST)
     public Integer getGamerelationCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return gamerelationMapper.getGamerelationCountByMap(param);
     }

     @RequestMapping(value = "/addGamerelation",method = RequestMethod.POST)
     public Integer addGamerelation(@RequestBody Gamerelation gamerelation)throws Exception{
        gamerelation.setA1(new Date().toString());
        return gamerelationMapper.insertGamerelation(gamerelation);
     }

     @RequestMapping(value = "/modifyGamerelation",method = RequestMethod.POST)
     public Integer modifyGamerelation(@RequestBody Gamerelation gamerelation)throws Exception{
        gamerelation.setA2(new Date().toString());
        return gamerelationMapper.updateGamerelation(gamerelation);
     }
}
