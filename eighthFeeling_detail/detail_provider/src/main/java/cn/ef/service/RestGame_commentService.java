package cn.ef.service;

import cn.ef.pojo.detail.Game_comment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.ef.mapper.Game_commentMapper;


import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGame_commentService {

     @Resource
     private Game_commentMapper game_commentMapper;

     @RequestMapping(value = "/getGame_commentById",method = RequestMethod.POST)
     public Game_comment getGame_commentById(@RequestParam("id") Long id)throws Exception{
        return game_commentMapper.getGame_commentById(id);
     }

     @RequestMapping(value = "/getGame_commentListByMap",method = RequestMethod.POST)
     public List<Game_comment>	getGame_commentListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_commentMapper.getGame_commentListByMap(param);
     }

     @RequestMapping(value = "/getGame_commentCountByMap",method = RequestMethod.POST)
     public Integer getGame_commentCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_commentMapper.getGame_commentCountByMap(param);
     }

     @RequestMapping(value = "/addGame_comment",method = RequestMethod.POST)
     public Integer addGame_comment(@RequestBody Game_comment game_comment)throws Exception{
        game_comment.setDate(new Date());
        return game_commentMapper.insertGame_comment(game_comment);
     }

     @RequestMapping(value = "/modifyGame_comment",method = RequestMethod.POST)
     public Integer modifyGame_comment(@RequestBody Game_comment game_comment)throws Exception{
        game_comment.setA1(new Date().toString());
        return game_commentMapper.updateGame_comment(game_comment);
     }
}
