package cn.ef.service;

import cn.ef.pojo.detail.Gametype;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.GametypeMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGametypeService {

     @Resource
     private GametypeMapper gametypeMapper;

     @RequestMapping(value = "/getGametypeById",method = RequestMethod.POST)
     public Gametype getGametypeById(@RequestParam("id") Long id)throws Exception{
        return gametypeMapper.getGametypeById(id);
     }

     @RequestMapping(value = "/getGametypeListByMap",method = RequestMethod.POST)
     public List<Gametype>	getGametypeListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return gametypeMapper.getGametypeListByMap(param);
     }

     @RequestMapping(value = "/getGametypeCountByMap",method = RequestMethod.POST)
     public Integer getGametypeCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return gametypeMapper.getGametypeCountByMap(param);
     }

     @RequestMapping(value = "/addGametype",method = RequestMethod.POST)
     public Integer addGametype(@RequestBody Gametype gametype)throws Exception{
        gametype.setA1(new Date().toString());
        return gametypeMapper.insertGametype(gametype);
     }

     @RequestMapping(value = "/modifyGametype",method = RequestMethod.POST)
     public Integer modifyGametype(@RequestBody Gametype gametype)throws Exception{
        gametype.setA2(new Date().toString());
        return gametypeMapper.updateGametype(gametype);
     }
}
