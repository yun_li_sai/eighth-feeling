package cn.ef.service;

import cn.ef.pojo.detail.Detail;
import cn.ef.repository.DetailRepository;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.ef.mapper.DetailMapper;


import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestDetailService {

    @Resource
    private DetailMapper detailMapper;

    @Resource
    DetailRepository detailRepository;

    @RequestMapping(value = "/getDetailById", method = RequestMethod.POST)
    public Detail getDetailById(@RequestParam("id") Long id) throws Exception {
        return detailMapper.getDetailById(id);
    }

    @RequestMapping(value = "/getDetailListByMap", method = RequestMethod.POST)
    public List<Detail> getDetailListByMap(@RequestParam Map<String, Object> param) throws Exception {
        return detailMapper.getDetailListByMap(param);
    }

    @RequestMapping(value = "/getDetailCountByMap", method = RequestMethod.POST)
    public Integer getDetailCountByMap(@RequestParam Map<String, Object> param) throws Exception {
        return detailMapper.getDetailCountByMap(param);
    }

    @RequestMapping(value = "/addDetail", method = RequestMethod.POST)
    public Integer addDetail(@RequestBody Detail detail) throws Exception {
        detail.setReleaseTime(new Date());
        return detailMapper.insertDetail(detail);
    }

    @RequestMapping(value = "/updateDetail", method = RequestMethod.POST)
    public Integer updateDetail(@RequestBody Detail detail) throws Exception {
        detail.setUpdateTime(new Date());
        return detailMapper.updateDetail(detail);
    }

    /**
     * 初始化s搜索引擎
     */
    @RequestMapping("init")
    public int init() throws Exception {
        List<Detail> list = detailMapper.getDetailListByMap(null);
        for (Detail detail : list) {
            detailRepository.save(detail);
        }
        return 1;
    }

    /**
     * 搜索
     * @param gameName
     * @return
     * @throws ParseException
     */
    @RequestMapping("/serach")
    public List<Detail> serach(String gameName) throws ParseException {
        //创建builder
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        //builder下有must、should以及mustNot 相当于sql中的and、or以及not
        //设置模糊搜索
        builder.must(QueryBuilders.fuzzyQuery("gameName", gameName));
        //按照id从高到低
        FieldSortBuilder sort = SortBuilders.fieldSort("id").order(SortOrder.DESC);
        //设置分页(拿第一页，一页显示两条)
        //注意!es的分页api是从第0页开始的(坑)
        PageRequest page = new PageRequest(0, 20);
        //构建查询
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        //将搜索条件设置到构建中
        nativeSearchQueryBuilder.withQuery(builder);
        //将分页设置到构建中
        nativeSearchQueryBuilder.withPageable(page);
        //将排序设置到构建中
        nativeSearchQueryBuilder.withSort(sort);
        //生产NativeSearchQuery
        NativeSearchQuery query = nativeSearchQueryBuilder.build();
        //执行
        Page<Detail> search = detailRepository.search(query);

        //获取总条数(前端分页需要使用)
        int total = (int) search.getTotalElements();

        //获取查询到的数据内容
        List<Detail> detailList = search.getContent();
        List<Detail> detailList1 = new ArrayList<Detail>();

        for (Detail detail : detailList) {
            //把数据装入对象中
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//需要转化成的时间格式
            Date releaseTime = detail.getReleaseTime();
            Date updateTime = detail.getUpdateTime();
            long rightReleaseTime = (long) (releaseTime.getTime() + 8 * 60 * 60 * 1000);//添加时间
            long rightUpdateTime = (long) (updateTime.getTime() + 8 * 60 * 60 * 1000);//添加时间
            String createAt = sdf1.format(rightReleaseTime);
            detail.setReleaseTime(sdf1.parse(createAt));
            String updateAt = sdf1.format(rightUpdateTime);
            detail.setUpdateTime(sdf1.parse(updateAt));
            detailList1.add(detail);
        }
        return detailList1;
    }
}
