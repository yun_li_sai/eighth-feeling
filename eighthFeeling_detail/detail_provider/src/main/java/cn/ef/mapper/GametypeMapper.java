package cn.ef.mapper;

import cn.ef.pojo.detail.Gametype;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GametypeMapper {

	public Gametype getGametypeById(@Param(value = "id") Long id)throws Exception;

	public List<Gametype>	getGametypeListByMap(Map<String, Object> param)throws Exception;

	public Integer getGametypeCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGametype(Gametype gametype)throws Exception;

	public Integer updateGametype(Gametype gametype)throws Exception;

	public Integer deleteGametypeById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGametype(Map<String, List<String>> params);

}
