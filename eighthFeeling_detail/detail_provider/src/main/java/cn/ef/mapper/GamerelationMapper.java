package cn.ef.mapper;

import cn.ef.pojo.detail.Gamerelation;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GamerelationMapper {

	public Gamerelation getGamerelationById(@Param(value = "id") Long id)throws Exception;

	public List<Gamerelation>	getGamerelationListByMap(Map<String, Object> param)throws Exception;

	public Integer getGamerelationCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGamerelation(Gamerelation gamerelation)throws Exception;

	public Integer updateGamerelation(Gamerelation gamerelation)throws Exception;

	public Integer deleteGamerelationById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGamerelation(Map<String, List<String>> params);

}
