package cn.ef.mapper;
import cn.ef.pojo.detail.Logging;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoggingMapper {

	public Logging getLoggingById(@Param(value = "id") Long id)throws Exception;

	public List<Logging>	getLoggingListByMap(Map<String, Object> param)throws Exception;

	public Integer getLoggingCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertLogging(Logging logging)throws Exception;

	public Integer updateLogging(Logging logging)throws Exception;

	public Integer deleteLoggingById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteLogging(Map<String, List<String>> params);

	public Integer exist(@Param("informationId") Long informationId);
}
