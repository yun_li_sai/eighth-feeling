package cn.ef.mapper;

import cn.ef.pojo.detail.Game_comment;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Game_commentMapper {

	public Game_comment getGame_commentById(@Param(value = "id") Long id)throws Exception;

	public List<Game_comment>	getGame_commentListByMap(Map<String, Object> param)throws Exception;

	public Integer getGame_commentCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGame_comment(Game_comment game_comment)throws Exception;

	public Integer updateGame_comment(Game_comment game_comment)throws Exception;

	public Integer deleteGame_commentById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGame_comment(Map<String, List<String>> params);

}
