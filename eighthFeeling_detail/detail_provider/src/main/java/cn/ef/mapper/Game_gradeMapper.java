package cn.ef.mapper;

import cn.ef.pojo.detail.Game_grade;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Game_gradeMapper {

	public Game_grade getGame_gradeById(@Param(value = "id") Long id)throws Exception;

	public List<Game_grade>	getGame_gradeListByMap(Map<String, Object> param)throws Exception;

	public Integer getGame_gradeCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGame_grade(Game_grade game_grade)throws Exception;

	public Integer updateGame_grade(Game_grade game_grade)throws Exception;

	public Integer deleteGame_gradeById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGame_grade(Map<String, List<String>> params);

}
