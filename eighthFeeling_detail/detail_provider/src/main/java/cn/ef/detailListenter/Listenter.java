package cn.ef.detailListenter;

import cn.ef.mapper.DetailMapper;
import cn.ef.mapper.LoggingMapper;
import cn.ef.pojo.detail.Detail;
import cn.ef.pojo.detail.Logging;
import org.apache.activemq.command.ActiveMQMapMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.jms.JMSException;
import java.util.HashMap;
import java.util.Map;

@Component
//取消收藏监听器
public class Listenter {

    @Resource
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Resource
    LoggingMapper loggingMapper;

    @Resource
    DetailMapper detailMapper;

    /**
     * 监听collect-msg
     *
     * @param payload
     */
    @JmsListener(destination = "collect-msg")
    @Transactional
    public void lstestlistener(Object payload) throws Exception {
        Map map = new HashMap();
        map = ((ActiveMQMapMessage) payload).getContentMap();
        Detail detail = new Detail();
        detail.setId((Long) map.get("id"));
        detail.setAttention_count(-1);
        Long informationId = (Long) map.get("informationId");
        int count = loggingMapper.exist(informationId);
        if (count > 0) {
                /*
                如果本地表中已存在该数据说明本地更新已完成，向消息对列发送通知
                 */
            jmsMessagingTemplate.convertAndSend(new ActiveMQQueue("information-msg"), informationId);
        } else {
            Logging logging = new Logging();
            logging.setInformationId(informationId);
            detailMapper.updateDetail(detail);
            loggingMapper.insertLogging(logging);
        }

    }
}
