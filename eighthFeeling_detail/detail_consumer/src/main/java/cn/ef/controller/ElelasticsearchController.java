package cn.ef.controller;

import cn.ef.client.detail.RestDetailClient;
import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import cn.ef.pojo.detail.Detail;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;

import java.util.List;

@RestController
public class ElelasticsearchController {

    @Resource
    RestDetailClient restDetailClient;

    /**
     * 搜索
     * @param gameName
     * @return
     * @throws ParseException
     */
    @RequestMapping("/serach")
    public Dto serach(String gameName) throws ParseException {
        List<Detail>list = restDetailClient.serach(gameName);
        if(list!=null&&list.size()>0){
            return DtoUtil.returnDataSuccess(list);
        }
        return DtoUtil.returnFail("获取失败","2222");
    }
}
