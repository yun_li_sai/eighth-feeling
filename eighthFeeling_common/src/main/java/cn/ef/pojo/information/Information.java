package cn.ef.pojo.information;
import java.io.Serializable;

/***
*   
*/
public class Information implements Serializable {
    //
    private Long informationId;
    //
    private String title;
    //
    private String detaile;
    //
    private Long gameId;
    //
    private Long picture;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setInformationId (Long  informationId){
        this.informationId=informationId;
    }
    public  Long getInformationId(){
        return this.informationId;
    }
    public void setTitle (String  title){
        this.title=title;
    }
    public  String getTitle(){
        return this.title;
    }
    public void setDetaile (String  detaile){
        this.detaile=detaile;
    }
    public  String getDetaile(){
        return this.detaile;
    }
    public void setGameId (Long  gameId){
        this.gameId=gameId;
    }
    public  Long getGameId(){
        return this.gameId;
    }
    public void setPicture (Long  picture){
        this.picture=picture;
    }
    public  Long getPicture(){
        return this.picture;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
