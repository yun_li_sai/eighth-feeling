package cn.ef.pojo.information;
import java.io.Serializable;
import java.util.Date;
/***
*   
*/
public class Activity implements Serializable {
    //
    private Long activityId;
    //
    private String activityName;
    //
    private Long gameId;
    //
    private Long pagination;
    //
    private Date startTime;
    //
    private Date endTime;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setActivityId (Long  activityId){
        this.activityId=activityId;
    }
    public  Long getActivityId(){
        return this.activityId;
    }
    public void setActivityName (String  activityName){
        this.activityName=activityName;
    }
    public  String getActivityName(){
        return this.activityName;
    }
    public void setGameId (Long  gameId){
        this.gameId=gameId;
    }
    public  Long getGameId(){
        return this.gameId;
    }
    public void setPagination (Long  pagination){
        this.pagination=pagination;
    }
    public  Long getPagination(){
        return this.pagination;
    }
    public void setStartTime (Date  startTime){
        this.startTime=startTime;
    }
    public  Date getStartTime(){
        return this.startTime;
    }
    public void setEndTime (Date  endTime){
        this.endTime=endTime;
    }
    public  Date getEndTime(){
        return this.endTime;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
