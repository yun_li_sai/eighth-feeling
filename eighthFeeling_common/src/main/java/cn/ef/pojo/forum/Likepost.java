package cn.ef.pojo.forum;
import java.io.Serializable;
import java.util.Date;

/***
*   
*/
public class Likepost implements Serializable {
    //帖子点赞表id
    private Long id;
    //用户id
    private Long userId;
    //帖子id
    private Long postId;
    //1代表点赞0代表未点赞
    private Integer state;
    //第一次点赞的时间
    private Date createTime;
    //修改时间
    private Date updateTime;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setPostId (Long  postId){
        this.postId=postId;
    }
    public  Long getPostId(){
        return this.postId;
    }
    public void setState (Integer  state){
        this.state=state;
    }
    public  Integer getState(){
        return this.state;
    }
    public void setCreateTime (Date  createTime){
        this.createTime=createTime;
    }
    public  Date getCreateTime(){
        return this.createTime;
    }
    public void setUpdateTime (Date  updateTime){
        this.updateTime=updateTime;
    }
    public  Date getUpdateTime(){
        return this.updateTime;
    }
}
