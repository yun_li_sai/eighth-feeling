package cn.ef.pojo.forum;
import java.io.Serializable;
import java.util.Date;

/***
*   
*/
public class Likecomment implements Serializable {
    //评论点赞表id
    private Long id;
    //用户id
    private Long userId;
    //评论表id
    private Long commentId;
    //1代表点赞0代表取消点赞
    private Integer state;
    //最开始点赞的时间
    private Date createTime;
    //修改时间
    private Date updateTime;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setCommentId (Long  commentId){
        this.commentId=commentId;
    }
    public  Long getCommentId(){
        return this.commentId;
    }
    public void setState (Integer  state){
        this.state=state;
    }
    public  Integer getState(){
        return this.state;
    }
    public void setCreateTime (Date  createTime){
        this.createTime=createTime;
    }
    public  Date getCreateTime(){
        return this.createTime;
    }
    public void setUpdateTime (Date  updateTime){
        this.updateTime=updateTime;
    }
    public  Date getUpdateTime(){
        return this.updateTime;
    }
}
