package cn.ef.pojo.forum;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
/***
*   
*/
public class Post implements Serializable {
    //帖子id
    private Long id;
    //游戏id
    private Long gameId;
    //游戏名称
    private String gameName;
    //用户id
    private Long userId;
    //用户名称
    private String userName;
    //标题
    private String title;
    //内容
    private String content;
    //发帖子时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //评论数
    private Long comments;
    //查看数
    private Long views;
    //回复数
    private Long replies;
    //点赞数
    private Long likes;
    //视频地址
    private String videoPath;
    //图片地址
    private String imagePath;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setGameId (Long  gameId){
        this.gameId=gameId;
    }
    public  Long getGameId(){
        return this.gameId;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setTitle (String  title){
        this.title=title;
    }
    public  String getTitle(){
        return this.title;
    }
    public void setContent (String  content){
        this.content=content;
    }
    public  String getContent(){
        return this.content;
    }
    public void setCreateTime (Date  createTime){
        this.createTime=createTime;
    }
    public  Date getCreateTime(){
        return this.createTime;
    }
    public void setComments (Long  comments){
        this.comments=comments;
    }
    public  Long getComments(){
        return this.comments;
    }
    public void setViews (Long  views){
        this.views=views;
    }
    public  Long getViews(){
        return this.views;
    }
    public void setReplies (Long  replies){
        this.replies=replies;
    }
    public  Long getReplies(){
        return this.replies;
    }
    public void setLikes (Long  likes){
        this.likes=likes;
    }
    public  Long getLikes(){
        return this.likes;
    }
    public void setVideoPath (String  videoPath){
        this.videoPath=videoPath;
    }
    public  String getVideoPath(){
        return this.videoPath;
    }
    public void setImagePath (String  imagePath){
        this.imagePath=imagePath;
    }
    public  String getImagePath(){
        return this.imagePath;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
