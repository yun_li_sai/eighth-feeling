package cn.ef.pojo.detail;
import java.io.Serializable;
import java.util.Date;
/***
*   评分表
*/
public class Game_grade implements Serializable {
    //评分id
    private Long gradeId;
    //游戏id
    private Long gameId;
    //用户id
    private Long userId;
    //评分时间
    private Date gradeTime;
    //评分分数
    private Integer gradeScore;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setGradeId (Long  gradeId){
        this.gradeId=gradeId;
    }
    public  Long getGradeId(){
        return this.gradeId;
    }
    public void setGameId (Long  gameId){
        this.gameId=gameId;
    }
    public  Long getGameId(){
        return this.gameId;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setGradeTime (Date  gradeTime){
        this.gradeTime=gradeTime;
    }
    public  Date getGradeTime(){
        return this.gradeTime;
    }
    public void setGradeScore (Integer  gradeScore){
        this.gradeScore=gradeScore;
    }
    public  Integer getGradeScore(){
        return this.gradeScore;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
