package cn.ef.pojo.detail;
import java.io.Serializable;
import java.util.Date;
/***
*   游戏评论表
*/
public class Game_comment implements Serializable {
    //id
    private Long id;
    //用户id
    private Long userid;
    //游戏id
    private Long gameid;
    //日期
    private Date date;
    //评论内容
    private String comment_content;
    //评论点赞数
    private Integer give_count;
    //所属类型
    private Integer classification;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserid (Long  userid){
        this.userid=userid;
    }
    public  Long getUserid(){
        return this.userid;
    }
    public void setGameid (Long  gameid){
        this.gameid=gameid;
    }
    public  Long getGameid(){
        return this.gameid;
    }
    public void setDate (Date  date){
        this.date=date;
    }
    public  Date getDate(){
        return this.date;
    }
    public void setComment_content (String  comment_content){
        this.comment_content=comment_content;
    }
    public  String getComment_content(){
        return this.comment_content;
    }
    public void setGive_count (Integer  give_count){
        this.give_count=give_count;
    }
    public  Integer getGive_count(){
        return this.give_count;
    }
    public void setClassification (Integer  classification){
        this.classification=classification;
    }
    public  Integer getClassification(){
        return this.classification;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
