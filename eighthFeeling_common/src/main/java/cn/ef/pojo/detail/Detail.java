package cn.ef.pojo.detail;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.lang.annotation.Documented;
import java.util.Date;
/***
*   游戏表
*/
@Document(indexName = "eighthfeeling_detail",type = "detail")
public class Detail implements Serializable {
    //id
    @Id
    private Long id;
    //厂家id
    private Long vender;
    //分类id
    private Long classify;
    //游戏名字
    private String gameName;
    //关注
    private Integer attention;
    //关注量
    private Integer attention_count;
    //视频剪接地址
    private String video_adress;
    //游戏图标地址
    private String game_adress;
    //发布日期
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date releaseTime;
    //是否上线
    private Integer logged;
    //评论数
    private Integer number_count;
    //查看数
    private Integer examine;
    //评分
    private Integer grade;
    //更新日期
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
    //游戏下载地址
    private String download_adress;
    //下载量
    private Integer download_count;
    //价格
    private Double price;
    //简介
    private String synopsis;
    //文件大小
    private String fileSize;
    //当前版本
    private String currentVersion;
    //厂商
    private String manufacturer;
    //状态
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setVender (Long  vender){
        this.vender=vender;
    }
    public  Long getVender(){
        return this.vender;
    }
    public void setClassify (Long  classify){
        this.classify=classify;
    }
    public  Long getClassify(){
        return this.classify;
    }
    public void setGameName (String  gameName){
        this.gameName=gameName;
    }
    public  String getGameName(){
        return this.gameName;
    }
    public void setAttention (Integer  attention){
        this.attention=attention;
    }
    public  Integer getAttention(){
        return this.attention;
    }
    public void setAttention_count (Integer  attention_count){
        this.attention_count=attention_count;
    }
    public  Integer getAttention_count(){
        return this.attention_count;
    }
    public void setVideo_adress (String  video_adress){
        this.video_adress=video_adress;
    }
    public  String getVideo_adress(){
        return this.video_adress;
    }
    public void setGame_adress (String  game_adress){
        this.game_adress=game_adress;
    }
    public  String getGame_adress(){
        return this.game_adress;
    }
    public void setReleaseTime (Date  releaseTime){
        this.releaseTime=releaseTime;
    }
    public  Date getReleaseTime(){
        return this.releaseTime;
    }
    public void setLogged (Integer  logged){
        this.logged=logged;
    }
    public  Integer getLogged(){
        return this.logged;
    }
    public void setNumber_count (Integer  number_count){
        this.number_count=number_count;
    }
    public  Integer getNumber_count(){
        return this.number_count;
    }
    public void setExamine (Integer  examine){
        this.examine=examine;
    }
    public  Integer getExamine(){
        return this.examine;
    }
    public void setGrade (Integer  grade){
        this.grade=grade;
    }
    public  Integer getGrade(){
        return this.grade;
    }
    public void setUpdateTime (Date  updateTime){
        this.updateTime=updateTime;
    }
    public  Date getUpdateTime(){
        return this.updateTime;
    }
    public void setDownload_adress (String  download_adress){
        this.download_adress=download_adress;
    }
    public  String getDownload_adress(){
        return this.download_adress;
    }
    public void setDownload_count (Integer  download_count){
        this.download_count=download_count;
    }
    public  Integer getDownload_count(){
        return this.download_count;
    }
    public void setPrice (Double  price){
        this.price=price;
    }
    public  Double getPrice(){
        return this.price;
    }
    public void setSynopsis (String  synopsis){
        this.synopsis=synopsis;
    }
    public  String getSynopsis(){
        return this.synopsis;
    }
    public void setFileSize (String  fileSize){
        this.fileSize=fileSize;
    }
    public  String getFileSize(){
        return this.fileSize;
    }
    public void setCurrentVersion (String  currentVersion){
        this.currentVersion=currentVersion;
    }
    public  String getCurrentVersion(){
        return this.currentVersion;
    }
    public void setManufacturer (String  manufacturer){
        this.manufacturer=manufacturer;
    }
    public  String getManufacturer(){
        return this.manufacturer;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }

}
