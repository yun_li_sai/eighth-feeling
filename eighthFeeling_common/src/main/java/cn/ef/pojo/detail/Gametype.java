package cn.ef.pojo.detail;
import java.io.Serializable;
import java.util.Date;
/***
*   游戏类型表
*/
public class Gametype implements Serializable {
    //类型id
    private Long classId;
    //类型名称
    private String className;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setClassId (Long  classId){
        this.classId=classId;
    }
    public  Long getClassId(){
        return this.classId;
    }
    public void setClassName (String  className){
        this.className=className;
    }
    public  String getClassName(){
        return this.className;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
