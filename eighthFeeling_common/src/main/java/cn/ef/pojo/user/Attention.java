package cn.ef.pojo.user;
import java.io.Serializable;
import java.util.Date;
/***
*   关注表
*/
public class Attention implements Serializable {
    //id
    private Long id;
    //用户id
    private Long userId;
    //目标id
    private Long targetId;
    //0为用户（关注） 1为游戏（收藏）
    private Integer type;
    //关注时间
    private Date attentionTime;
    //关注状态 0关注 1取消关注
    private Integer state;
    //修改时间
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setTargetId (Long   targetId){
        this.targetId= targetId;
    }
    public  Long getTargetId(){
        return this.targetId;
    }
    public void setType (Integer  type){
        this.type=type;
    }
    public  Integer getType(){
        return this.type;
    }
    public void setAttentionTime (Date  attentionTime){
        this.attentionTime=attentionTime;
    }
    public  Date getAttentionTime(){
        return this.attentionTime;
    }
    public void setState (Integer  state){
        this.state=state;
    }
    public  Integer getState(){
        return this.state;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
