package cn.ef.pojo.user;
import java.io.Serializable;
import java.util.Date;
/***
*   用户表
*/
public class User implements Serializable {
    //账户id
    private Long id;
    //用户名
    private String userName;
    //密码
    private String password;
    //手机号
    private String phone;
    //性别 0男，1女
    private Integer gender;
    //国家/地区
    private Long manufacturer;
    //邮箱
    private String email;
    //微信号
    private String weChatID;
    //qq号
    private Integer qq;
    //用户类型 0管理员 1会员
    private Integer type;
    //个性签名
    private String signature;
    //关注数 默认为0
    private Integer attentionNumber;
    //收藏数 默认为0
    private Integer collectionNumber;
    //注销状态 0未注销 1已注销
    private Integer state;
    //粉丝数 默认为0
    private Integer numberOfFans;
    //是否实名认证 0未认证 1已认证 默认为0
    private Integer realNameAuthentication;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserName (String  userName){
        this.userName=userName;
    }
    public  String getUserName(){
        return this.userName;
    }
    public void setPassword (String  password){
        this.password=password;
    }
    public  String getPassword(){
        return this.password;
    }
    public void setPhone (String  phone){
        this.phone=phone;
    }
    public  String getPhone(){
        return this.phone;
    }
    public void setGender (Integer  gender){
        this.gender=gender;
    }
    public  Integer getGender(){
        return this.gender;
    }
    public void setManufacturer (Long  manufacturer){
        this.manufacturer=manufacturer;
    }
    public  Long getManufacturer(){
        return this.manufacturer;
    }
    public void setEnail (String  enail){
        this.email=enail;
    }
    public  String getEnail(){
        return this.email;
    }
    public void setWeChatID (String  weChatID){
        this.weChatID=weChatID;
    }
    public  String getWeChatID(){
        return this.weChatID;
    }
    public void setQq (Integer  qq){
        this.qq=qq;
    }
    public  Integer getQq(){
        return this.qq;
    }
    public void setType (Integer  type){
        this.type=type;
    }
    public  Integer getType(){
        return this.type;
    }
    public void setSignature (String  signature){
        this.signature=signature;
    }
    public  String getSignature(){
        return this.signature;
    }
    public void setAttentionNumber (Integer  attentionNumber){
        this.attentionNumber=attentionNumber;
    }
    public  Integer getAttentionNumber(){
        return this.attentionNumber;
    }
    public void setCollectionNumber (Integer  collectionNumber){
        this.collectionNumber=collectionNumber;
    }
    public  Integer getCollectionNumber(){
        return this.collectionNumber;
    }
    public void setState (Integer  state){
        this.state=state;
    }
    public  Integer getState(){
        return this.state;
    }
    public void setNumberOfFans (Integer  numberOfFans){
        this.numberOfFans=numberOfFans;
    }
    public  Integer getNumberOfFans(){
        return this.numberOfFans;
    }
    public void setRealNameAuthentication (Integer  realNameAuthentication){
        this.realNameAuthentication=realNameAuthentication;
    }
    public  Integer getRealNameAuthentication(){
        return this.realNameAuthentication;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
