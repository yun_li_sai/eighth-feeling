package cn.ef.pojo.user;
import java.io.Serializable;
import java.util.Date;
/***
*   消息表
*/
public class Information implements Serializable {
    //消息表Id
    private Long id;
    //用户Id
    private Long userId;
    //游戏Id
    private Long targetId;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setTargetId (Long  targetId){
        this.targetId=targetId;
    }
    public  Long getTargetId(){
        return this.targetId;
    }
}
