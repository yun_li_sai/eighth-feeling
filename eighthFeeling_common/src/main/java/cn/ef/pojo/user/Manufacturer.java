package cn.ef.pojo.user;
import java.io.Serializable;
import java.util.Date;
/***
*   游戏厂商表
*/
public class Manufacturer implements Serializable {
    //厂家id
    private Long id;
    //游戏厂商地址
    private String address;
    //厂家名称
    private String vendorName;
    //厂家官网地址
    private String contactUrl;
    //简介
    private String introduce;
    //联系人姓名
    private String contactName;
    //联系人电话
    private Integer contactPhone;
    //是否通过资格认证 0未通过，1通过
    private Integer certification;
    //粉丝数 默认为0
    private Integer numberOfFans;
    //logo图片地址
    private String logo;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setAddress (String  address){
        this.address=address;
    }
    public  String getAddress(){
        return this.address;
    }
    public void setVendorName (String  vendorName){
        this.vendorName=vendorName;
    }
    public  String getVendorName(){
        return this.vendorName;
    }
    public void setContactUrl (String  contactUrl){
        this.contactUrl=contactUrl;
    }
    public  String getContactUrl(){
        return this.contactUrl;
    }
    public void setIntroduce (String  introduce){
        this.introduce=introduce;
    }
    public  String getIntroduce(){
        return this.introduce;
    }
    public void setContactName (String  contactName){
        this.contactName=contactName;
    }
    public  String getContactName(){
        return this.contactName;
    }
    public void setContactPhone (Integer   contactPhone){
        this.contactPhone= contactPhone;
    }
    public  Integer getContactPhone(){
        return this.contactPhone;
    }
    public void setCertification (Integer  certification){
        this.certification=certification;
    }
    public  Integer getCertification(){
        return this.certification;
    }
    public void setNumberOfFans (Integer  numberOfFans){
        this.numberOfFans=numberOfFans;
    }
    public  Integer getNumberOfFans(){
        return this.numberOfFans;
    }
    public void setLogo (String  logo){
        this.logo=logo;
    }
    public  String getLogo(){
        return this.logo;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
