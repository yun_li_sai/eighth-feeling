package cn.ef.pojo.record;
import java.io.Serializable;
import java.util.Date;
/***
*   
*/
public class Game_history implements Serializable {
    //id
    private Long id;
    //用户Id
    private Long userId;
    //创建时间
    private Date creatorTime;
    //目标Id
    private Long targetid;
    //内容
    private String content;
    //类型,1点赞记录,2搜索记录,3下载记录
    private Integer type;
    //
    private String A5;
    //
    private String A4;
    //
    private String A6;
    //get set 方法
    public void setId (Long  id){
        this.id=id;
    }
    public  Long getId(){
        return this.id;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setCreatorTime (Date  creatorTime){
        this.creatorTime=creatorTime;
    }
    public  Date getCreatorTime(){
        return this.creatorTime;
    }
    public void setTargetid (Long  targetid){
        this.targetid=targetid;
    }
    public  Long getTargetid(){
        return this.targetid;
    }
    public void setContent (String  content){
        this.content=content;
    }
    public  String getContent(){
        return this.content;
    }
    public void setType (Integer  type){
        this.type=type;
    }
    public  Integer getType(){
        return this.type;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA6 (String  A6){
        this.A6=A6;
    }
    public  String getA6(){
        return this.A6;
    }
}
