package cn.ef.pojo.record;
import java.io.Serializable;
import java.util.Date;
/***
*   
*/
public class Game_record_buyorder implements Serializable {
    //订单id
    private Long Id;
    //订单单号
    private String oddnumbers;
    //游戏id
    private Long gameId;
    //用户id
    private Long userId;
    //支付状态0.已付款 1.未付款
    private Integer paystate;
    //订单日期
    private Date orderdate;
    //冻结金额
    private Long freeze;
    //订单金额
    private Long ordermoney;
    //
    private String A1;
    //
    private String A2;
    //
    private String A3;
    //
    private String A4;
    //
    private String A5;
    //get set 方法
    public void setId (Long  Id){
        this.Id=Id;
    }
    public  Long getId(){
        return this.Id;
    }
    public void setOddnumbers (String  oddnumbers){
        this.oddnumbers=oddnumbers;
    }
    public  String getOddnumbers(){
        return this.oddnumbers;
    }
    public void setGameId (Long  gameId){
        this.gameId=gameId;
    }
    public  Long getGameId(){
        return this.gameId;
    }
    public void setUserId (Long  userId){
        this.userId=userId;
    }
    public  Long getUserId(){
        return this.userId;
    }
    public void setPaystate (Integer  paystate){
        this.paystate=paystate;
    }
    public  Integer getPaystate(){
        return this.paystate;
    }
    public void setOrderdate (Date  orderdate){
        this.orderdate=orderdate;
    }
    public  Date getOrderdate(){
        return this.orderdate;
    }
    public void setFreeze (Long  freeze){
        this.freeze=freeze;
    }
    public  Long getFreeze(){
        return this.freeze;
    }
    public void setOrdermoney (Long  ordermoney){
        this.ordermoney=ordermoney;
    }
    public  Long getOrdermoney(){
        return this.ordermoney;
    }
    public void setA1 (String  A1){
        this.A1=A1;
    }
    public  String getA1(){
        return this.A1;
    }
    public void setA2 (String  A2){
        this.A2=A2;
    }
    public  String getA2(){
        return this.A2;
    }
    public void setA3 (String  A3){
        this.A3=A3;
    }
    public  String getA3(){
        return this.A3;
    }
    public void setA4 (String  A4){
        this.A4=A4;
    }
    public  String getA4(){
        return this.A4;
    }
    public void setA5 (String  A5){
        this.A5=A5;
    }
    public  String getA5(){
        return this.A5;
    }
}
