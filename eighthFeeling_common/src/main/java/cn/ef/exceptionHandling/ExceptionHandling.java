package cn.ef.exceptionHandling;

import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//全局异常处理类
@RestControllerAdvice
public class ExceptionHandling {

    /**
     * 处理全部异常类型
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Dto handling(){
        return DtoUtil.returnFail("出现异常","0000");
    }
}
