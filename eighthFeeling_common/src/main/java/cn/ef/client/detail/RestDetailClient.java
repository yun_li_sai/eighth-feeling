package cn.ef.client.detail;
import cn.ef.fallback.detail.DetailClientFallBack;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import cn.ef.pojo.detail.Detail;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = DetailClientFallBack.class)
public interface RestDetailClient {



    @RequestMapping(value = "/getDetailById",method = RequestMethod.POST)
    public Detail getDetailById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getDetailListByMap",method = RequestMethod.POST)
    public List<Detail>	getDetailListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getDetailCountByMap",method = RequestMethod.POST)
    public Integer getDetailCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addDetail",method = RequestMethod.POST)
    public Integer addDetail(@RequestBody Detail detail)throws Exception;

    @RequestMapping(value = "/updateDetail",method = RequestMethod.POST)
    public Integer updateDetail(@RequestBody Detail detail)throws Exception;

    @RequestMapping("/serach")
    public List<Detail>serach(@RequestParam("gameName")String gameName) throws ParseException;
}
