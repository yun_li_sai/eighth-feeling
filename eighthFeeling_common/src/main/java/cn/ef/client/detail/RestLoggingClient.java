package cn.ef.client.detail;

import java.util.List;
import java.util.Map;

import cn.ef.fallback.detail.LoggingClientFallBack;
import cn.ef.pojo.detail.Logging;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = LoggingClientFallBack.class)
public interface RestLoggingClient {

    @RequestMapping(value = "/getLoggingById",method = RequestMethod.POST)
    public Logging getLoggingById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getLoggingListByMap",method = RequestMethod.POST)
    public List<Logging>	getLoggingListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getLoggingCountByMap",method = RequestMethod.POST)
    public Integer getLoggingCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addLogging",method = RequestMethod.POST)
    public Integer addLogging(@RequestBody Logging logging)throws Exception;

    @RequestMapping(value = "/modifyLogging",method = RequestMethod.POST)
    public Integer modifyLogging(@RequestBody Logging logging)throws Exception;
}
