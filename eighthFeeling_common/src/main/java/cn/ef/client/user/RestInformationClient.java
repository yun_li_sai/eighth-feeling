package cn.ef.client.user;
import java.util.List;
import java.util.Map;

import cn.ef.fallback.user.InformationClientFallBack;
import cn.ef.pojo.user.Information;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = InformationClientFallBack.class)
public interface RestInformationClient {

    @RequestMapping(value = "/getInformationById",method = RequestMethod.POST)
    public Information getInformationById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getInformationListByMap",method = RequestMethod.POST)
    public List<Information>	getInformationListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getInformationCountByMap",method = RequestMethod.POST)
    public Integer getInformationCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addInformation",method = RequestMethod.POST)
    public Integer addInformation(@RequestBody Information information)throws Exception;

    @RequestMapping(value = "/modifyInformation",method = RequestMethod.POST)
    public Integer modifyInformation(@RequestBody Information information)throws Exception;
}
