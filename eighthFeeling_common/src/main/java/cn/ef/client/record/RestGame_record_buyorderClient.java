package cn.ef.client.record;

import java.util.List;
import java.util.Map;

import cn.ef.fallback.record.Game_record_buyorderClientFallBack;
import cn.ef.pojo.record.Game_record_buyorder;
import feign.Param;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = Game_record_buyorderClientFallBack.class)
public interface RestGame_record_buyorderClient {

    @RequestMapping(value = "/getGame_record_buyorderById",method = RequestMethod.POST)
    public Game_record_buyorder getGame_record_buyorderById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getGame_record_buyorderListByMap",method = RequestMethod.POST)
    public List<Game_record_buyorder>	getGame_record_buyorderListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getGame_record_buyorderCountByMap",method = RequestMethod.POST)
    public Integer getGame_record_buyorderCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addGame_record_buyorder",method = RequestMethod.POST)
    public Integer addGame_record_buyorder(@RequestBody Game_record_buyorder game_record_buyorder)throws Exception;

    @RequestMapping(value = "/modifyGame_record_buyorder",method = RequestMethod.POST)
    public Integer modifyGame_record_buyorder(@RequestBody Game_record_buyorder game_record_buyorder)throws Exception;

    @RequestMapping("/getBuyorderByUserId")
    public List<Game_record_buyorder>getBuyorderByUserId(@Param("id")Long id);
}
