package cn.ef.client.record;

import java.util.List;
import java.util.Map;

import cn.ef.fallback.record.Game_historyClientFallBack;
import cn.ef.pojo.record.Game_history;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = Game_historyClientFallBack.class)
public interface RestGame_historyClient {

    @RequestMapping(value = "/getGame_historyById",method = RequestMethod.POST)
    public Game_history getGame_historyById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getGame_historyListByMap",method = RequestMethod.POST)
    public List<Game_history>	getGame_historyListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getGame_historyCountByMap",method = RequestMethod.POST)
    public Integer getGame_historyCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addGame_history",method = RequestMethod.POST)
    public Integer addGame_history(@RequestBody Game_history game_history)throws Exception;

    @RequestMapping(value = "/modifyGame_history",method = RequestMethod.POST)
    public Integer modifyGame_history(@RequestBody Game_history game_history)throws Exception;

    @RequestMapping(value = "/hotHistory",method = RequestMethod.POST)
    public List<Game_history>hotHistory(@RequestParam("userId")Long userId);
}
