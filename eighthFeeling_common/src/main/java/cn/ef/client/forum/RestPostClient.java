package cn.ef.client.forum;
import cn.ef.fallback.forum.PostClientFallBack;

import java.util.List;
import java.util.Map;

import cn.ef.pojo.forum.Post;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = PostClientFallBack.class)
public interface RestPostClient {

    @RequestMapping(value = "/getPostById",method = RequestMethod.POST)
    public Post getPostById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getPostListByMap",method = RequestMethod.POST)
    public List<Post>	getPostListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getPostCountByMap",method = RequestMethod.POST)
    public Integer getPostCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addPost",method = RequestMethod.POST)
    public Integer addPost(@RequestBody Post post)throws Exception;

    @RequestMapping(value = "/modifyPost",method = RequestMethod.POST)
    public Integer modifyPost(@RequestBody Post post)throws Exception;



}
