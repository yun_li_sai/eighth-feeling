package cn.ef.client.forum;
import cn.ef.fallback.forum.LikepostClientFallBack;

import java.util.List;
import java.util.Map;

import cn.ef.pojo.forum.Likepost;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "",  fallback = LikepostClientFallBack.class)
public interface RestLikepostClient {

    @RequestMapping(value = "/getLikepostById",method = RequestMethod.POST)
    public Likepost getLikepostById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getLikepostListByMap",method = RequestMethod.POST)
    public List<Likepost>	getLikepostListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getLikepostCountByMap",method = RequestMethod.POST)
    public Integer getLikepostCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addLikepost",method = RequestMethod.POST)
    public Integer addLikepost(@RequestBody Likepost likepost)throws Exception;

    @RequestMapping(value = "/modifyLikepost",method = RequestMethod.POST)
    public Integer modifyLikepost(@RequestBody Likepost likepost)throws Exception;
}
