package cn.ef.util;



import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {
    private RedisTemplate redisTemplate;

    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate){
        Jackson2JsonRedisSerializer json=new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om=new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        json.setObjectMapper(om);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(json);
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(json);
        this.redisTemplate=redisTemplate;
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void set(String key, Object value){
        redisTemplate.opsForValue().set(key,value);
    }

    public Object get(String key){

        return redisTemplate.opsForValue().get(key);
    }
    public boolean haskey(String key){

        return redisTemplate.hasKey(key);
    }

    public void lpush(String key,Object value){
        redisTemplate.opsForList().leftPush(key,value);
    }
    public  long lsize(String key){
        return redisTemplate.opsForList().size(key);
    }
    public  Object lget(String key,long index){
        return redisTemplate.opsForList().index(key,index);
    }
    public  Object lgetAll(String key,long size){
        return redisTemplate.opsForList().range(key,0,size);
    }


    public void hpush(String key,Object value){
        redisTemplate.opsForHash().putAll(key,(Map)value);
    }
    public  Object hget(String key){
        return redisTemplate.opsForHash().entries(key);
    }
    public void hput(String key,String hkey,Object value){
        redisTemplate.opsForHash().put(key,hkey,value);
    }
    public  Object hgetone(String key,String hkey){
        return redisTemplate.opsForHash().hasKey(key,hkey);
    }
    public boolean hhaskey(String key,String hkey){
        return  redisTemplate.opsForHash().hasKey(key,hkey);
    }


    public  boolean setnx(String key,String value,long expiretime){
       boolean result= redisTemplate.opsForValue().setIfAbsent(key,value);
       if(result==true){
           redisTemplate.expire(key,expiretime, TimeUnit.SECONDS);
       }
       return  result;
    }

    public  void delete(String key){
        redisTemplate.delete(key);
    }

    public boolean expire(String key,long expiretime){
       return redisTemplate.expire(key,expiretime,TimeUnit.SECONDS);
    }

    public long ttl(String key){
        return redisTemplate.getExpire(key,TimeUnit.SECONDS);
    }





}
