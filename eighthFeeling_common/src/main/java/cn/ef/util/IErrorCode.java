package cn.ef.util;

public interface IErrorCode {
    public String getErrorCode();
    public String getErrorMessage();
}
