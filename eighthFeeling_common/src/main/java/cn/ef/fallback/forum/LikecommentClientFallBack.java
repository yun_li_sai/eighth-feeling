package cn.ef.fallback.forum;


import cn.ef.client.forum.RestLikecommentClient;
import cn.ef.pojo.forum.Likecomment;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class LikecommentClientFallBack implements RestLikecommentClient {


    @Override
    public Likecomment getLikecommentById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Likecomment>	getLikecommentListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getLikecommentCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addLikecomment(Likecomment likecomment)throws Exception{
        return null;
    }

    @Override
    public Integer modifyLikecomment(Likecomment likecomment)throws Exception{
        return null;
    }
}
