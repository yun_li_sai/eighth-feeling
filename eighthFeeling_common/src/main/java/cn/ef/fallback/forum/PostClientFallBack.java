package cn.ef.fallback.forum;

import cn.ef.client.forum.RestPostClient;
import cn.ef.pojo.forum.Post;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class PostClientFallBack implements RestPostClient {


    @Override
    public Post getPostById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Post>	getPostListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getPostCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addPost(Post post)throws Exception{
        return null;
    }

    @Override
    public Integer modifyPost(Post post)throws Exception{
        return null;
    }

    @Override
    public List<Post> newPostByAttention(List<Long> list) {
        return null;
    }
}
