package cn.ef.fallback.user;

import cn.ef.client.user.RestInformationClient;
import cn.ef.pojo.user.Information;

import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class InformationClientFallBack implements RestInformationClient {


    @Override
    public Information getInformationById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Information>	getInformationListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getInformationCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addInformation(Information information)throws Exception{
        return null;
    }

    @Override
    public Integer modifyInformation(Information information)throws Exception{
        return null;
    }
}
