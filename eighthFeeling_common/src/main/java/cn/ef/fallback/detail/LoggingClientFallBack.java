package cn.ef.fallback.detail;
import cn.ef.client.detail.RestLoggingClient;
import cn.ef.pojo.detail.Logging;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class LoggingClientFallBack implements RestLoggingClient {


    @Override
    public Logging getLoggingById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Logging>	getLoggingListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getLoggingCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addLogging(Logging logging)throws Exception{
        return null;
    }

    @Override
    public Integer modifyLogging(Logging logging)throws Exception{
        return null;
    }
}
