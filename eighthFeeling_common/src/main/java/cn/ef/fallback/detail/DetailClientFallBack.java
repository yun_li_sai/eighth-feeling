package cn.ef.fallback.detail;

import cn.ef.client.detail.RestDetailClient;

import cn.ef.pojo.detail.Detail;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
@Component
public class DetailClientFallBack implements RestDetailClient {


    @Override
    public Detail getDetailById(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Detail>	getDetailListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getDetailCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addDetail(Detail detail)throws Exception{
        return null;
    }

    @Override
    public Integer modifyDetail(Detail detail)throws Exception{
        return null;
    }

    @Override
    public List<Detail> serach(String gameName) throws ParseException {
        return null;
    }
}
