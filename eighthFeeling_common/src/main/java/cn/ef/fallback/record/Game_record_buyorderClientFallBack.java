package cn.ef.fallback.record;

import cn.ef.client.record.RestGame_record_buyorderClient;

import cn.ef.pojo.record.Game_record_buyorder;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class Game_record_buyorderClientFallBack implements RestGame_record_buyorderClient {


    @Override
    public Game_record_buyorder getGame_record_buyorderById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Game_record_buyorder>	getGame_record_buyorderListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getGame_record_buyorderCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addGame_record_buyorder(Game_record_buyorder game_record_buyorder)throws Exception{
        return null;
    }

    @Override
    public Integer modifyGame_record_buyorder(Game_record_buyorder game_record_buyorder)throws Exception{
        return null;
    }

    @Override
    public List<Game_record_buyorder> getBuyorderByUserId(Long id) {
        return null;
    }
}
