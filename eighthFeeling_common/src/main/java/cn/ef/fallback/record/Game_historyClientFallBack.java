package cn.ef.fallback.record;

import cn.ef.client.record.RestGame_historyClient;

import cn.ef.pojo.record.Game_history;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
@Component
public class Game_historyClientFallBack implements RestGame_historyClient {


    @Override
    public Game_history getGame_historyById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Game_history>	getGame_historyListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getGame_historyCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addGame_history(Game_history game_history)throws Exception{
        return null;
    }

    @Override
    public Integer modifyGame_history(Game_history game_history)throws Exception{
        return null;
    }
}
