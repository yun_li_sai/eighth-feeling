package cn.ef.mapper;

import cn.ef.pojo.record.Game_record_buyorder;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Game_record_buyorderMapper {

	public Game_record_buyorder getGame_record_buyorderById(@Param(value = "id") Long id)throws Exception;

	public List<Game_record_buyorder>	getGame_record_buyorderListByMap(Map<String, Object> param)throws Exception;

	public Integer getGame_record_buyorderCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGame_record_buyorder(Game_record_buyorder game_record_buyorder)throws Exception;

	public Integer updateGame_record_buyorder(Game_record_buyorder game_record_buyorder)throws Exception;

	public Integer deleteGame_record_buyorderById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGame_record_buyorder(Map<String, List<String>> params);

	public List<Game_record_buyorder> getBuyorderByUserId(@Param(value = "id")Long id);
}
