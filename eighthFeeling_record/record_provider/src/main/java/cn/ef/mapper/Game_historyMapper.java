package cn.ef.mapper;

import cn.ef.pojo.record.Game_history;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Game_historyMapper {

	public Game_history getGame_historyById(@Param(value = "id") Long id)throws Exception;

	public List<Game_history>	getGame_historyListByMap(Map<String, Object> param)throws Exception;

	public Integer getGame_historyCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertGame_history(Game_history game_history)throws Exception;

	public Integer updateGame_history(Game_history game_history)throws Exception;

	public Integer deleteGame_historyById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteGame_history(Map<String, List<String>> params);

	public List<Game_history>hotHistory(@Param(value = "userId")Long userId);
}
