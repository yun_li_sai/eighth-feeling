package cn.ef.service;

import cn.ef.pojo.record.Game_history;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.Game_historyMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGame_historyService {

     @Resource
     private Game_historyMapper game_historyMapper;

     @RequestMapping(value = "/getGame_historyById",method = RequestMethod.POST)
     public Game_history getGame_historyById(@RequestParam("id") Long id)throws Exception{
        return game_historyMapper.getGame_historyById(id);
     }

     @RequestMapping(value = "/getGame_historyListByMap",method = RequestMethod.POST)
     public List<Game_history>	getGame_historyListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_historyMapper.getGame_historyListByMap(param);
     }

     @RequestMapping(value = "/getGame_historyCountByMap",method = RequestMethod.POST)
     public Integer getGame_historyCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_historyMapper.getGame_historyCountByMap(param);
     }

     @RequestMapping(value = "/addGame_history",method = RequestMethod.POST)
     public Integer addGame_history(@RequestBody Game_history game_history)throws Exception{
        game_history.setCreatorTime(new Date());
        return game_historyMapper.insertGame_history(game_history);
     }

     @RequestMapping(value = "/modifyGame_history",method = RequestMethod.POST)
     public Integer modifyGame_history(@RequestBody Game_history game_history)throws Exception{
        game_history.setA4(new Date().toString());
        return game_historyMapper.updateGame_history(game_history);
     }
     @RequestMapping("/hotHistory")
    public List<Game_history>hotHistory(Long userId){
         return game_historyMapper.hotHistory(userId);
     }
}
