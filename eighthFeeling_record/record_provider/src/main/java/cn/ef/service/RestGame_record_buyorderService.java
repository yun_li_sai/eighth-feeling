package cn.ef.service;

import cn.ef.pojo.record.Game_record_buyorder;
;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.ef.mapper.Game_record_buyorderMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestGame_record_buyorderService {

     @Resource
     private Game_record_buyorderMapper game_record_buyorderMapper;

     @RequestMapping(value = "/getGame_record_buyorderById",method = RequestMethod.POST)
     public Game_record_buyorder getGame_record_buyorderById(@RequestParam("id") Long id)throws Exception{
        return game_record_buyorderMapper.getGame_record_buyorderById(id);
     }

     @RequestMapping(value = "/getGame_record_buyorderListByMap",method = RequestMethod.POST)
     public List<Game_record_buyorder>	getGame_record_buyorderListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_record_buyorderMapper.getGame_record_buyorderListByMap(param);
     }

     @RequestMapping(value = "/getGame_record_buyorderCountByMap",method = RequestMethod.POST)
     public Integer getGame_record_buyorderCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return game_record_buyorderMapper.getGame_record_buyorderCountByMap(param);
     }

     @RequestMapping(value = "/addGame_record_buyorder",method = RequestMethod.POST)
     public Integer addGame_record_buyorder(@RequestBody Game_record_buyorder game_record_buyorder)throws Exception{
        game_record_buyorder.setA1(new Date().toString());
        return game_record_buyorderMapper.insertGame_record_buyorder(game_record_buyorder);
     }

     @RequestMapping(value = "/modifyGame_record_buyorder",method = RequestMethod.POST)
     public Integer modifyGame_record_buyorder(@RequestBody Game_record_buyorder game_record_buyorder)throws Exception{
         game_record_buyorder.setA2(new Date().toString());
         return game_record_buyorderMapper.updateGame_record_buyorder(game_record_buyorder);
     }

    @RequestMapping("/getBuyorderByUserId")
    public List<Game_record_buyorder>getBuyorderByUserId(Long id){
        return game_record_buyorderMapper.getBuyorderByUserId(id);
    }
}
