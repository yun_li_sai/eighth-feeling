package cn.ef.controller;

import cn.ef.client.record.RestGame_historyClient;
import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;

import cn.ef.pojo.record.Game_history;
import cn.ef.util.RedisUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GameHistoryController {

    @Resource
    RestGame_historyClient gamehistoryClient;

    @Resource
    RedisUtil redisUtils;

    /**
     * 查看全部下载记录
     * @param userId
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/allGameHistory")
    public Dto allGameHistory(Long userId,@RequestHeader("token")String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if(!result){
            Object a =new Object();

            return DtoUtil.returnFail("请先登录","1111");
        }else {
            Map map = new HashMap();
            map.put("userId",userId);
            map.put("beginPos",0);
            map.put("pageSize",20);
            List<Game_history> list = gamehistoryClient.getGame_historyListByMap(map);
            if (list!=null&&list.size()>0){
                return DtoUtil.returnDataSuccess(list);
            }else {
                return DtoUtil.returnFail("获取失败","2222");
            }
        }
    }

    /**
     * 热门标签（搜索次数多的游戏）
     * @param userId
     * @return
     */
    @RequestMapping("/hotHistory")
    public Dto searchHistory(Long userId) throws Exception {
       List<Game_history> list= gamehistoryClient.hotHistory(userId);
       if(list!=null&&list.size()>0){
           return DtoUtil.returnDataSuccess(list);
       }else {
           return DtoUtil.returnFail("获取失败","2222");
       }
    }

    /**
     *搜索记录
     */
    public Dto getSearchHistory(Long userId) throws Exception {
        Map map = new HashMap();
        map.put("userId",userId);
        map.put("type",2);
        map.put("beginPos",0);
        map.put("pageSize",10);
        List<Game_history>list = gamehistoryClient.getGame_historyListByMap(map);
        if(list!=null&&list.size()>0){
            return DtoUtil.returnDataSuccess(list);
        }else {
            return DtoUtil.returnFail("获取失败","2222");
        }
    }
}
