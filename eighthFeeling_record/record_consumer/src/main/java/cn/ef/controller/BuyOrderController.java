package cn.ef.controller;

import cn.ef.client.record.RestGame_record_buyorderClient;
import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import cn.ef.pojo.record.Game_record_buyorder;
import cn.ef.util.RedisUtil;


import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/buyOrder")
public class BuyOrderController {

    @Resource
    RestGame_record_buyorderClient buyorderClient;



    @Resource
    RedisUtil redisUtils;

    /**
     * 获取对应用户的全部订单
     * @param id
     * @param token
     * @return
     */
    @RequestMapping("/getBuyorderByUserId")
    public Dto getBuyorderByUserId(Long id,@RequestHeader("token") String token){
        boolean result = redisUtils.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            List<Game_record_buyorder> list= buyorderClient.getBuyorderByUserId(id);
            if (list==null||list.size()<1){
                return DtoUtil.returnFail("获取失败","2222");
            }
            return DtoUtil.returnDataSuccess(list);
        }
    }

    /**
     * 查看订单详情
     * @param id
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/buyOrderDetail")
    public Dto BuyOrderDetail(Long id,@RequestHeader("token")String token) throws Exception {
        Game_record_buyorder buyorder = buyorderClient.getGame_record_buyorderById(id);
        boolean result = redisUtils.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            if (buyorder!=null){
                return DtoUtil.returnDataSuccess(buyorder);
            }else{
                return DtoUtil.returnFail("获取失败","2222");
            }

        }

    }
}
