package cn.ef.service;

import cn.ef.pojo.information.Information;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.InformationMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestInformationService {

     @Resource
     private InformationMapper informationMapper;

     @RequestMapping(value = "/getInformationById",method = RequestMethod.POST)
     public Information getInformationById(@RequestParam("id") Long id)throws Exception{
        return informationMapper.getInformationById(id);
     }

     @RequestMapping(value = "/getInformationListByMap",method = RequestMethod.POST)
     public List<Information>	getInformationListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return informationMapper.getInformationListByMap(param);
     }

     @RequestMapping(value = "/getInformationCountByMap",method = RequestMethod.POST)
     public Integer getInformationCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return informationMapper.getInformationCountByMap(param);
     }

     @RequestMapping(value = "/addInformation",method = RequestMethod.POST)
     public Integer addInformation(@RequestBody Information information)throws Exception{
        information.setA1(new Date().toString());
        return informationMapper.insertInformation(information);
     }

     @RequestMapping(value = "/modifyInformation",method = RequestMethod.POST)
     public Integer modifyInformation(@RequestBody Information information)throws Exception{
        information.setA2(new Date().toString());
        return informationMapper.updateInformation(information);
     }
}
