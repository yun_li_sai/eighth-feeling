package cn.ef.service;

import cn.ef.pojo.information.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import cn.ef.mapper.ActivityMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestActivityService {

     @Resource
     private ActivityMapper activityMapper;

     @RequestMapping(value = "/getActivityById",method = RequestMethod.POST)
     public Activity getActivityById(@RequestParam("id") Long id)throws Exception{
        return activityMapper.getActivityById(id);
     }

     @RequestMapping(value = "/getActivityListByMap",method = RequestMethod.POST)
     public List<Activity>	getActivityListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return activityMapper.getActivityListByMap(param);
     }

     @RequestMapping(value = "/getActivityCountByMap",method = RequestMethod.POST)
     public Integer getActivityCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return activityMapper.getActivityCountByMap(param);
     }

     @RequestMapping(value = "/addActivity",method = RequestMethod.POST)
     public Integer addActivity(@RequestBody Activity activity)throws Exception{
        activity.setA1(new Date().toString());
        return activityMapper.insertActivity(activity);
     }

     @RequestMapping(value = "/modifyActivity",method = RequestMethod.POST)
     public Integer modifyActivity(@RequestBody Activity activity)throws Exception{
        activity.setA2(new Date().toString());
        return activityMapper.updateActivity(activity);
     }
}
