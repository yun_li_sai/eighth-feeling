package cn.ef.mapper;

import cn.ef.pojo.information.Activity;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityMapper {

	public Activity getActivityById(@Param(value = "id") Long id)throws Exception;

	public List<Activity>	getActivityListByMap(Map<String, Object> param)throws Exception;

	public Integer getActivityCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertActivity(Activity activity)throws Exception;

	public Integer updateActivity(Activity activity)throws Exception;

	public Integer deleteActivityById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteActivity(Map<String, List<String>> params);

}
