package cn.ef.fallback;

import cn.ef.client.LocalGameClient;

import cn.ef.pojo.detail.Detail;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LocalGameClientFallBack implements LocalGameClient {

    @Override
    public Detail getGameById(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Detail> getGamesTop() throws Exception {
        return null;
    }
}
