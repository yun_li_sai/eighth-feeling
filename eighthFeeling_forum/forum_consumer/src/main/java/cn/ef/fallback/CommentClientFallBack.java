package cn.ef.fallback;

import cn.ef.client.RestCommentClient;

import cn.ef.pojo.forum.Comment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CommentClientFallBack implements RestCommentClient {


    @Override
    public Comment getCommentById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Comment>	getCommentListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getCommentCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addComment(Comment comment)throws Exception{
        return null;
    }

    @Override
    public Integer modifyComment(Comment comment)throws Exception{
        return null;
    }

    @Override
    public Integer addLikesComment(Long id) throws Exception {
        return null;
    }

    @Override
    public Integer reduceLikesComment(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Comment> getCommentsByPostId(Long postId) throws Exception {
        return null;
    }


}
