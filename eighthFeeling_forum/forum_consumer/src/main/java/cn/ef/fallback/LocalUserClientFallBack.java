package cn.ef.fallback;

import cn.ef.client.LocalUserClient;

import cn.ef.pojo.user.User;
import org.springframework.stereotype.Component;

@Component
public class LocalUserClientFallBack implements LocalUserClient {
    @Override
    public User getUserById(Long id) throws Exception {
        return null;
    }
}
