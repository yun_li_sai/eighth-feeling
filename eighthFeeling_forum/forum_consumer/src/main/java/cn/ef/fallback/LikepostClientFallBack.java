package cn.ef.fallback;

import cn.ef.client.RestLikepostClient;

import cn.ef.pojo.forum.Likepost;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LikepostClientFallBack implements RestLikepostClient {


    @Override
    public Likepost getLikepostById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Likepost>	getLikepostListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getLikepostCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addLikepost(Likepost likepost)throws Exception{
        return null;
    }

    @Override
    public Integer modifyLikepost(Likepost likepost)throws Exception{
        return null;
    }

    @Override
    public Likepost getLikePostCount(Long userId, Long postId) throws Exception {
        return null;
    }

    @Override
    public Integer cancelLikePost(Likepost likepost) throws Exception {
        return null;
    }

    @Override
    public Integer againLikePost(Likepost likepost) throws Exception {
        return null;
    }


}
