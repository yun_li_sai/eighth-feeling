package cn.ef.client;

import cn.ef.fallback.LikecommentClientFallBack;
import cn.ef.pojo.forum.Likecomment;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "forum-provider",  fallback = LikecommentClientFallBack.class)
public interface RestLikecommentClient {

    @RequestMapping(value = "/getLikecommentById",method = RequestMethod.POST)
    public Likecomment getLikecommentById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getLikecommentListByMap",method = RequestMethod.POST)
    public List<Likecomment>	getLikecommentListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getLikecommentCountByMap",method = RequestMethod.POST)
    public Integer getLikecommentCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addLikecomment",method = RequestMethod.POST)
    public Integer addLikecomment(@RequestBody Likecomment likecomment)throws Exception;

    @RequestMapping(value = "/modifyLikecomment",method = RequestMethod.POST)
    public Integer modifyLikecomment(@RequestBody Likecomment likecomment)throws Exception;

    @RequestMapping(value = "/getLikeCommentCount",method = RequestMethod.POST)
    public Likecomment getLikeCommentCount(@RequestParam("userId") Long userId, @RequestParam("commentId") Long commentId)throws Exception;

    @RequestMapping(value = "/cancelLikeComment",method = RequestMethod.POST)
    public Integer cancelLikeComment(@RequestBody Likecomment likecomment)throws Exception;

    @RequestMapping(value = "/againLikeComment",method = RequestMethod.POST)
    public Integer againLikeComment(@RequestBody Likecomment likecomment)throws Exception;

}
