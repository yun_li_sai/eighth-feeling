package cn.ef.client;

import cn.ef.fallback.LocalUserClientFallBack;

import cn.ef.pojo.user.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-provider",  fallback = LocalUserClientFallBack.class)
public interface LocalUserClient {

    @RequestMapping(value = "/getUserById",method = RequestMethod.POST)
    public User getUserById(@RequestParam("id") Long id)throws Exception;
}
