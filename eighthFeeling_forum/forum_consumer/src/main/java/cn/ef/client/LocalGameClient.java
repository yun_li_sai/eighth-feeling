package cn.ef.client;

import cn.ef.fallback.LocalGameClientFallBack;

import cn.ef.pojo.detail.Detail;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "game-provider",  fallback = LocalGameClientFallBack.class)
public interface LocalGameClient {

    @RequestMapping(value = "/getGameById",method = RequestMethod.POST)
    public Detail getGameById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getGamesTop",method = RequestMethod.POST)
    public List<Detail> getGamesTop()throws Exception;


}
