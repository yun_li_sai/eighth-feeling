package cn.ef.client;

import cn.ef.fallback.PostClientFallBack;

import cn.ef.pojo.forum.Post;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "forum-provider",  fallback = PostClientFallBack.class)
public interface RestPostClient {

    @RequestMapping(value = "/getPostById",method = RequestMethod.POST)
    public Post getPostById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getPostListByMap",method = RequestMethod.POST)
    public List<Post>	getPostListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getPostCountByMap",method = RequestMethod.POST)
    public Integer getPostCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addPost",method = RequestMethod.POST)
    public Integer addPost(@RequestBody Post post)throws Exception;

    @RequestMapping(value = "/modifyPost",method = RequestMethod.POST)
    public Integer modifyPost(@RequestBody Post post)throws Exception;

    @RequestMapping(value = "/addLikesPost",method = RequestMethod.POST)
    public Integer addLikesPost(@RequestParam("id") Long id)throws Exception;
    @RequestMapping(value = "/reduceLikesPost",method = RequestMethod.POST)
    public Integer reduceLikesPost(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getPostsByGameId",method = RequestMethod.POST)
    public List<Post> getPostsByGameId(@RequestParam("gameId") Long gameId)throws Exception;
}
