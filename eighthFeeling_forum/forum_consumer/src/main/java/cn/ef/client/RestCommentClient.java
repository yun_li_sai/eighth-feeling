package cn.ef.client;

import cn.ef.fallback.CommentClientFallBack;

import cn.ef.pojo.forum.Comment;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "forum-provider",  fallback = CommentClientFallBack.class)
public interface RestCommentClient {

    @RequestMapping(value = "/getCommentById",method = RequestMethod.POST)
    public Comment getCommentById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getCommentListByMap",method = RequestMethod.POST)
    public List<Comment>	getCommentListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getCommentCountByMap",method = RequestMethod.POST)
    public Integer getCommentCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addComment",method = RequestMethod.POST)
    public Integer addComment(@RequestBody Comment comment)throws Exception;

    @RequestMapping(value = "/modifyComment",method = RequestMethod.POST)
    public Integer modifyComment(@RequestBody Comment comment)throws Exception;

    @RequestMapping(value = "/addLikesComment",method = RequestMethod.POST)
    public Integer addLikesComment(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/reduceLikesComment",method = RequestMethod.POST)
    public Integer reduceLikesComment(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getCommentsByPostId",method = RequestMethod.POST)
    public List<Comment> getCommentsByPostId(@RequestParam("postId") Long postId)throws Exception;
}
