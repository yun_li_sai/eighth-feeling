package cn.ef.controller;


import cn.ef.client.LocalGameClient;
import cn.ef.client.LocalUserClient;
import cn.ef.client.RestLikepostClient;
import cn.ef.client.RestPostClient;
import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import cn.ef.pojo.detail.Detail;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
public class GameController {
    @Resource
    private RestPostClient restPostClient;
    @Resource
    private LocalUserClient localUserClient;
    @Resource
    private LocalGameClient localGameClient;
    @Resource
    private RestLikepostClient likepostClient;

    //展示贴吧前四
    @RequestMapping(value = "/getGameTop",method = RequestMethod.POST)
    public Dto getGameTop()throws Exception{
        List<Detail> list=localGameClient.getGamesTop();
       return DtoUtil.returnDataSuccess(list);
    }


    //根据游戏id查询贴吧的基本介绍
    @RequestMapping(value = "/getGameById",method = RequestMethod.POST)
    public Dto getGameById(Long gameId)throws Exception{
        Detail game=localGameClient.getGameById(gameId);
        return DtoUtil.returnDataSuccess(game);
    }





}
