package cn.ef.controller;


import cn.ef.client.LocalGameClient;
import cn.ef.client.LocalUserClient;
import cn.ef.client.RestLikepostClient;
import cn.ef.client.RestPostClient;
import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import cn.ef.pojo.detail.Detail;
import cn.ef.pojo.forum.Likepost;
import cn.ef.pojo.forum.Post;
import cn.ef.pojo.user.User;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
public class PostController {
    @Resource
    private RestPostClient restPostClient;
    @Resource
    private LocalUserClient localUserClient;
    @Resource
    private LocalGameClient localGameClient;
    @Resource
    private RestLikepostClient likepostClient;

    //发帖
    @RequestMapping(value = "/addPost",method = RequestMethod.POST)
    public Dto addPost(Post post)throws Exception{
        Integer count=restPostClient.addPost(post);
        if(count>0){
            return DtoUtil.returnDataSuccess(count);
        }else {
            return DtoUtil.returnFail("add fail","400");
        }
    }

    //根据id查贴
    @RequestMapping(value = "/getPostById",method = RequestMethod.POST)
    public Dto getPostById(Long id)throws Exception{
        Post post=restPostClient.getPostById(id);
        User user=localUserClient.getUserById(post.getUserId());
        Detail game=localGameClient.getGameById(post.getGameId());
        post.setUserName(user.getUserName());
        post.setGameName(game.getGameName());
        return DtoUtil.returnDataSuccess(post);
    }

    //查询用户是否对该帖子点赞过
    @RequestMapping(value = "/checkLikePost",method = RequestMethod.POST)
    public Dto checkLikePost(Long userId, Long postId)throws Exception{
        Likepost lp=likepostClient.getLikePostCount(userId,postId);
        if(lp==null){
            return DtoUtil.returnFail("not have record","400");
        }
        return DtoUtil.returnDataSuccess(lp);
    }



    //给帖子点赞或者取消点赞
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    @RequestMapping(value = "/likePost",method = RequestMethod.POST)
    public Dto likePost(Long userId, Long postId)throws Exception{
        Likepost lp=likepostClient.getLikePostCount(userId,postId);
        Integer count=0;
        Integer number=0;
        if(lp==null){
            //新增点赞记录
            lp.setUserId(userId);
            lp.setPostId(postId);
            lp.setCreateTime(new Date());
            lp.setState(1);
            number=restPostClient.addLikesPost(postId);//点赞数+1
            count=likepostClient.addLikepost(lp);//新增点赞记录

        }else {
            if(lp.getState()==0){//已经取消过点赞的用户
                //重新点赞
                lp.setUpdateTime(new Date());
                lp.setState(1);
                count=likepostClient.againLikePost(lp);//修改点赞记录
                number=restPostClient.addLikesPost(postId);//点赞数+1
            }else {//已经点过赞的用户
                //取消点赞
                lp.setUpdateTime(new Date());
                lp.setState(0);
                count=likepostClient.cancelLikePost(lp);//修改点赞记录
                number=restPostClient.reduceLikesPost(postId);//点赞数-1
            }
        }
        if(count*number>0){
            return DtoUtil.returnDataSuccess(lp);
        }else {
            return  DtoUtil.returnFail("失败！","400");
        }
    }

    //根据gameId查询出对应的所有帖子
    @RequestMapping(value = "/getPostByGameId",method = RequestMethod.POST)
    public Dto getPostByGameId(@RequestParam("gameId") Long gameId)throws Exception {
        List<Post> list=restPostClient.getPostsByGameId(gameId);
        List<Post> posts= new ArrayList<Post>();
        for(Post post:list){
            User user=localUserClient.getUserById(post.getUserId());
            Detail game=localGameClient.getGameById(post.getGameId());
            post.setUserName(user.getUserName());
            post.setGameName(game.getGameName());
            posts.add(post);
        }
        return  DtoUtil.returnDataSuccess(posts);

    }


}
