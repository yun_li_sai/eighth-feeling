package cn.ef.controller;

import cn.ef.client.LocalUserClient;
import cn.ef.client.RestCommentClient;
import cn.ef.client.RestLikecommentClient;
import cn.ef.dto.Dto;

import cn.ef.dto.DtoUtil;
import cn.ef.pojo.forum.Comment;
import cn.ef.pojo.forum.Likecomment;
import cn.ef.pojo.user.User;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
public class CommentController {
    @Resource
    private RestCommentClient restCommentClient;
    @Resource
    private LocalUserClient localUserClient;
    @Resource
    private RestLikecommentClient restLikecommentClient;


    //发表评论
    @RequestMapping(value = "/addComment",method = RequestMethod.POST)
    public Dto addComment(Comment comment)throws Exception{
        Integer count=restCommentClient.addComment(comment);
        if(count>0){
            return DtoUtil.returnDataSuccess(count);
        }else {
            return DtoUtil.returnFail("add fail","400");
        }
    }


    //查询用户是否对该评论点赞过
    @RequestMapping(value = "/checkLike",method = RequestMethod.POST)
    public Dto checkLike(Long userId, Long commentId)throws Exception{
        Likecomment lc=restLikecommentClient.getLikeCommentCount(userId, commentId);
        if(lc==null){
           return DtoUtil.returnFail("not have record","400");
        }
        return DtoUtil.returnDataSuccess(lc);
    }



    //给评论点赞或者取消点赞
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    @RequestMapping(value = "/likeComment",method = RequestMethod.POST)
    public Dto likeComment(Long userId, Long commentId)throws Exception{
        Likecomment lc=restLikecommentClient.getLikeCommentCount(userId, commentId);
        Integer count=0;
        Integer number=0;
        if(lc==null){
            //新增点赞记录
            lc.setUserId(userId);
            lc.setCommentId(commentId);
            lc.setCreateTime(new Date());
            lc.setState(1);
            number=restCommentClient.addLikesComment(commentId);//点赞数+1
             count=restLikecommentClient.addLikecomment(lc);//新增点赞记录

        }else {
            if(lc.getState()==0){//已经取消过点赞的用户
                //重新点赞
                lc.setUpdateTime(new Date());
                lc.setState(1);
                 count=restLikecommentClient.againLikeComment(lc);//修改点赞记录
                 number=restCommentClient.addLikesComment(commentId);//点赞数+1
            }else {//已经点过赞的用户
                //取消点赞
                lc.setUpdateTime(new Date());
                lc.setState(0);
                 count=restLikecommentClient.cancelLikeComment(lc);//修改点赞记录
                 number=restCommentClient.reduceLikesComment(commentId);//点赞数-1
            }
        }
        if(count*number>0){
            return DtoUtil.returnDataSuccess(lc);
        }else {
            return  DtoUtil.returnFail("失败！","400");
        }
    }

    //根据postId查询出对应的所有评论
    @RequestMapping(value = "/getCommentsByPostId",method = RequestMethod.POST)
    public Dto getCommentsByPostId(@RequestParam("postId") Long postId)throws Exception {
        List<Comment> list=restCommentClient.getCommentsByPostId(postId);
        List<Comment> comments= new ArrayList<Comment>();
        for(Comment comment:list){
            User user=localUserClient.getUserById(comment.getUserId());
            comment.setUserName(user.getUserName());
            comments.add(comment);
        }
        return  DtoUtil.returnDataSuccess(comments);

    }



}
