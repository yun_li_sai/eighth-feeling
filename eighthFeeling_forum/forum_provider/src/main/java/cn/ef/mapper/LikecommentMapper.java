package cn.ef.mapper;


import cn.ef.pojo.forum.Likecomment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface LikecommentMapper {

	public Likecomment getLikecommentById(@Param(value = "id") Long id)throws Exception;

	public List<Likecomment>	getLikecommentListByMap(Map<String, Object> param)throws Exception;

	public Integer getLikecommentCountByMap(Map<String, Object> param)throws Exception;

	//新增点赞
	public Integer insertLikecomment(Likecomment likecomment)throws Exception;

	public Integer updateLikecomment(Likecomment likecomment)throws Exception;

	public Integer deleteLikecommentById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteLikecomment(Map<String, List<String>> params);

	//查询是否有该贴的点赞记录
	public Likecomment getLikeCommentCount(@Param(value = "userId") Long userId, @Param(value = "commentId") Long commentId);

	//取消点赞/重新点赞
	public Integer updateLikeState(Likecomment likecomment);


}
