package cn.ef.mapper;


import cn.ef.pojo.forum.Post;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PostMapper {

	public Post getPostById(@Param(value = "id") Long id)throws Exception;

	public List<Post>	getPostListByMap(Map<String, Object> param)throws Exception;

	public Integer getPostCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertPost(Post post)throws Exception;

	public Integer updatePost(Post post)throws Exception;

	public Integer deletePostById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeletePost(Map<String, List<String>> params);

	//点赞数+1
	public Integer addLikes(@Param(value = "id") Long id)throws Exception;
	//点赞数-1
	public Integer reduceLikes(@Param(value = "id") Long id)throws Exception;

	//根据游戏id查询所有该帖子
	public List<Post> getPostsByGameId(@Param(value = "gameId") Long gameId);

	//关注用户的最新动态
	public List<Post>newPostByAttention(List<Long> list);
}
