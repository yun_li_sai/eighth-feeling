package cn.ef.mapper;


import cn.ef.pojo.forum.Likepost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface LikepostMapper {

	public Likepost getLikepostById(@Param(value = "id") Long id)throws Exception;

	public List<Likepost>	getLikepostListByMap(Map<String, Object> param)throws Exception;

	public Integer getLikepostCountByMap(Map<String, Object> param)throws Exception;
	//新增点赞
	public Integer insertLikepost(Likepost likepost)throws Exception;

	public Integer updateLikepost(Likepost likepost)throws Exception;

	public Integer deleteLikepostById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteLikepost(Map<String, List<String>> params);

	//查询是否有该贴的点赞记录
	public Likepost getLikePostCount(@Param(value = "userId") Long userId, @Param(value = "postId") Long postId);

	//取消点赞/重新点赞
	public Integer updateLikeState(Likepost likepost);

}
