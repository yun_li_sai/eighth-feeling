package cn.ef.mapper;


import cn.ef.pojo.forum.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import cn.ef.pojo.forum.Comment;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentMapper {

	public Comment getCommentById(@Param(value = "id") Long id)throws Exception;

	public List<Comment>	getCommentListByMap(Map<String, Object> param)throws Exception;

	public Integer getCommentCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertComment(Comment comment)throws Exception;

	public Integer updateComment(Comment comment)throws Exception;

	public Integer deleteCommentById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteComment(Map<String, List<String>> params);

	//点赞数+1
	public Integer addLikes(@Param(value = "id") Long id)throws Exception;
	//点赞数-1
	public Integer reduceLikes(@Param(value = "id") Long id)throws Exception;

	//根据帖子id查询所有该帖子的评论
	public List<Comment> getCommentsByPostId(@Param(value = "postId") Long postId);


	public List<Comment> commentListByMapOrderByCreateTime(Map<String, Object> param);
}
