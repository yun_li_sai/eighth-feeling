package cn.ef.service;

import cn.ef.mapper.PostMapper;

import cn.ef.pojo.forum.Post;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestPostService {

     @Resource
     private PostMapper postMapper;

     @RequestMapping(value = "/getPostById",method = RequestMethod.POST)
     public Post getPostById(@RequestParam("id") Long id)throws Exception{
        return postMapper.getPostById(id);
     }

     @RequestMapping(value = "/getPostListByMap",method = RequestMethod.POST)
     public List<Post>	getPostListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return postMapper.getPostListByMap(param);
     }

     @RequestMapping(value = "/getPostCountByMap",method = RequestMethod.POST)
     public Integer getPostCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return postMapper.getPostCountByMap(param);
     }

     @RequestMapping(value = "/addPost",method = RequestMethod.POST)
     public Integer addPost(@RequestBody Post post)throws Exception{
        post.setCreateTime(new Date());
        post.setLikes(0L);
        post.setReplies(0L);
        post.setComments(0L);
        post.setViews(0l);
        return postMapper.insertPost(post);
     }

     @RequestMapping(value = "/modifyPost",method = RequestMethod.POST)
     public Integer modifyPost(@RequestBody Post post)throws Exception{
        return postMapper.updatePost(post);
     }


     @RequestMapping("/newPostByAttention")
     public List<Post>newPostByAttention(List<Long> list){
         return postMapper.newPostByAttention(list);
     }

    @RequestMapping(value = "/addLikesPost",method = RequestMethod.POST)
    public Integer addLikesPost(@RequestParam("id") Long id)throws Exception{
        return postMapper.addLikes(id);
    }
    @RequestMapping(value = "/reduceLikesPost",method = RequestMethod.POST)
    public Integer reduceLikesPost(@RequestParam("id") Long id)throws Exception{
        return postMapper.reduceLikes(id);
    }

    @RequestMapping(value = "/getPostsByGameId",method = RequestMethod.POST)
    public List<Post> getPostsByGameId(@RequestParam("gameId") Long gameId)throws Exception{
        return postMapper.getPostsByGameId(gameId);
    }



}
