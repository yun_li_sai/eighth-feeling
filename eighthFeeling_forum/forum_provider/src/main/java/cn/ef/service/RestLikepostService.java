package cn.ef.service;

import cn.ef.mapper.LikepostMapper;

import cn.ef.pojo.forum.Likepost;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestLikepostService {

     @Resource
     private LikepostMapper likepostMapper;

     @RequestMapping(value = "/getLikepostById",method = RequestMethod.POST)
     public Likepost getLikepostById(@RequestParam("id") Long id)throws Exception{
        return likepostMapper.getLikepostById(id);
     }

     @RequestMapping(value = "/getLikepostListByMap",method = RequestMethod.POST)
     public List<Likepost>	getLikepostListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return likepostMapper.getLikepostListByMap(param);
     }

     @RequestMapping(value = "/getLikepostCountByMap",method = RequestMethod.POST)
     public Integer getLikepostCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return likepostMapper.getLikepostCountByMap(param);
     }

     @RequestMapping(value = "/addLikepost",method = RequestMethod.POST)
     public Integer addLikepost(@RequestBody Likepost likepost)throws Exception{
        likepost.setCreateTime(new Date());
        return likepostMapper.insertLikepost(likepost);
     }

     @RequestMapping(value = "/modifyLikepost",method = RequestMethod.POST)
     public Integer modifyLikepost(@RequestBody Likepost likepost)throws Exception{
        likepost.setUpdateTime(new Date());
        return likepostMapper.updateLikepost(likepost);
     }

    @RequestMapping(value = "/getLikePostCount",method = RequestMethod.POST)
    public Likepost getLikePostCount(@RequestParam("userId") Long userId,@RequestParam("postId") Long postId)throws Exception{
        return likepostMapper.getLikePostCount(userId, postId);
    }

    @RequestMapping(value = "/cancelLikePost",method = RequestMethod.POST)
    public Integer cancelLikePost(@RequestBody Likepost likepost)throws Exception{
        likepost.setUpdateTime(new Date());
        likepost.setState(0);
        return likepostMapper.updateLikeState(likepost);
    }

    @RequestMapping(value = "/againLikePost",method = RequestMethod.POST)
    public Integer againLikePost(@RequestBody Likepost likepost)throws Exception{
        likepost.setUpdateTime(new Date());
        likepost.setState(1);
        return likepostMapper.updateLikeState(likepost);
    }
}
