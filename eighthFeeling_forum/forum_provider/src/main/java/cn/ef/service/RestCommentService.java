package cn.ef.service;

import cn.ef.mapper.CommentMapper;

import cn.ef.pojo.forum.Comment;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestCommentService {

     @Resource
     private CommentMapper commentMapper;

     @RequestMapping(value = "/getCommentById",method = RequestMethod.POST)
     public Comment getCommentById(@RequestParam("id") Long id)throws Exception{
        return commentMapper.getCommentById(id);
     }

     @RequestMapping(value = "/getCommentListByMap",method = RequestMethod.POST)
     public List<Comment>	getCommentListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return commentMapper.getCommentListByMap(param);
     }

     @RequestMapping(value = "/getCommentCountByMap",method = RequestMethod.POST)
     public Integer getCommentCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return commentMapper.getCommentCountByMap(param);
     }

     @RequestMapping(value = "/addComment",method = RequestMethod.POST)
     public Integer addComment(@RequestBody Comment comment)throws Exception{
         comment.setCreateTime(new Date());
         comment.setLikes(0l);
        return commentMapper.insertComment(comment);
     }

     @RequestMapping(value = "/modifyComment",method = RequestMethod.POST)
     public Integer modifyComment(@RequestBody Comment comment)throws Exception{
        return commentMapper.updateComment(comment);
     }

    @RequestMapping(value = "/addLikesComment",method = RequestMethod.POST)
    public Integer addLikesComment(@RequestParam("id") Long id)throws Exception{
        return commentMapper.addLikes(id);
    }
    @RequestMapping(value = "/reduceLikesComment",method = RequestMethod.POST)
    public Integer reduceLikesComment(@RequestParam("id") Long id)throws Exception{
        return commentMapper.reduceLikes(id);
    }

    @RequestMapping(value = "/getCommentsByPostId",method = RequestMethod.POST)
    public List<Comment> getCommentsByPostId(@RequestParam("postId") Long postId)throws Exception{
        return commentMapper.getCommentsByPostId(postId);

    @RequestMapping("/commentListByMapOrderByCreateTime")
    public List<Comment> commentListByMapOrderByCreateTime(@RequestBody Map<String, Object> param){
        return commentMapper.commentListByMapOrderByCreateTime(param);
    }
}
