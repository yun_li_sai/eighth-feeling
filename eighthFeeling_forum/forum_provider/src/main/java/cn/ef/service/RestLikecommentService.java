package cn.ef.service;

import cn.ef.mapper.LikecommentMapper;

import cn.ef.pojo.forum.Likecomment;
import cn.ef.pojo.forum.Likecomment;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.ef.mapper.LikecommentMapper;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestLikecommentService {

     @Resource
     private LikecommentMapper likecommentMapper;

     @RequestMapping(value = "/getLikecommentById",method = RequestMethod.POST)
     public Likecomment getLikecommentById(@RequestParam("id") Long id)throws Exception{
        return likecommentMapper.getLikecommentById(id);
     }

     @RequestMapping(value = "/getLikecommentListByMap",method = RequestMethod.POST)
     public List<Likecomment>	getLikecommentListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return likecommentMapper.getLikecommentListByMap(param);
     }

     @RequestMapping(value = "/getLikecommentCountByMap",method = RequestMethod.POST)
     public Integer getLikecommentCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return likecommentMapper.getLikecommentCountByMap(param);
     }

     @RequestMapping(value = "/addLikecomment",method = RequestMethod.POST)
     public Integer addLikecomment(@RequestBody Likecomment likecomment)throws Exception{
        likecomment.setCreateTime(new Date());
        return likecommentMapper.insertLikecomment(likecomment);
     }

     @RequestMapping(value = "/modifyLikecomment",method = RequestMethod.POST)
     public Integer modifyLikecomment(@RequestBody Likecomment likecomment)throws Exception{
        likecomment.setUpdateTime(new Date());
        return likecommentMapper.updateLikecomment(likecomment);
     }

    @RequestMapping(value = "/getLikeCommentCount",method = RequestMethod.POST)
    public Likecomment getLikeCommentCount(@RequestParam("userId") Long userId,@RequestParam("commentId") Long commentId)throws Exception{
        return likecommentMapper.getLikeCommentCount(userId,commentId);
    }

    @RequestMapping(value = "/cancelLikeComment",method = RequestMethod.POST)
    public Integer cancelLikeComment(@RequestBody Likecomment likecomment)throws Exception{
        likecomment.setUpdateTime(new Date());
        likecomment.setState(0);
        return likecommentMapper.updateLikeState(likecomment);
    }

    @RequestMapping(value = "/againLikeComment",method = RequestMethod.POST)
    public Integer againLikeComment(@RequestBody Likecomment likecomment)throws Exception{
        likecomment.setUpdateTime(new Date());
        likecomment.setState(1);
        return likecommentMapper.updateLikeState(likecomment);
    }

}
