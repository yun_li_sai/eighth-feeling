package cn.ef.task;

import cn.ef.mapper.InformationMapper;
import cn.ef.pojo.user.Information;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class QgMessageTask {

    @Resource
    InformationMapper informationMapper;

    @Resource
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Scheduled(fixedDelay=1000)
    public void task() throws Exception {
        Map map = new HashMap();
        List<Information> list = informationMapper.getInformationListByMap(null);
        for (Information information:list){

            map.put("id",information.getTargetId());
            map.put("informationId",information.getId());
            jmsMessagingTemplate.convertAndSend(new ActiveMQQueue("collect-msg"),map);
        }
    }
}
