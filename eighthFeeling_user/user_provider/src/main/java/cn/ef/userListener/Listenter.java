package cn.ef.userListener;


import cn.ef.mapper.InformationMapper;
import cn.ef.pojo.detail.Detail;
import cn.ef.pojo.detail.Logging;
import cn.ef.pojo.user.Information;
import org.apache.activemq.command.ActiveMQMapMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.jms.JMSException;
import java.util.HashMap;
import java.util.Map;

@Component
public class Listenter {

    @Resource
    InformationMapper informationMapper;

    @JmsListener(destination = "information-msg")
    public void lstestlistener(Object payload){
        String information = null;
        try {
            information = ((ActiveMQTextMessage)payload).getText();
            informationMapper.deleteInformationById(Long.parseLong(information));
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
