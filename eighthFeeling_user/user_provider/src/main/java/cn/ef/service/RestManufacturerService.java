package cn.ef.service;

import cn.ef.mapper.ManufacturerMapper;

import cn.ef.pojo.user.Manufacturer;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RestManufacturerService {

     @Resource
     private ManufacturerMapper manufacturerMapper;

     @RequestMapping(value = "/getManufacturerById",method = RequestMethod.POST)
     public Manufacturer getManufacturerById(@RequestParam("id") Long id)throws Exception{
        return manufacturerMapper.getManufacturerById(id);
     }

     @RequestMapping(value = "/getManufacturerListByMap",method = RequestMethod.POST)
     public List<Manufacturer>	getManufacturerListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return manufacturerMapper.getManufacturerListByMap(param);
     }

     @RequestMapping(value = "/getManufacturerCountByMap",method = RequestMethod.POST)
     public Integer getManufacturerCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return manufacturerMapper.getManufacturerCountByMap(param);
     }

     @RequestMapping(value = "/addManufacturer",method = RequestMethod.POST)
     public Integer addManufacturer(@RequestBody Manufacturer manufacturer)throws Exception{
        return manufacturerMapper.insertManufacturer(manufacturer);
     }

     @RequestMapping(value = "/modifyManufacturer",method = RequestMethod.POST)
     public Integer modifyManufacturer(@RequestBody Manufacturer manufacturer)throws Exception{
        return manufacturerMapper.updateManufacturer(manufacturer);
     }
}
