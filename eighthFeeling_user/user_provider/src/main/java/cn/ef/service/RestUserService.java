package cn.ef.service;

import cn.ef.mapper.AttentionMapper;
import cn.ef.mapper.UserMapper;

import cn.ef.pojo.user.Attention;
import cn.ef.pojo.user.User;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RestUserService {

     @Resource
     private UserMapper userMapper;

     @Resource
     private AttentionMapper attentionMapper;

     @RequestMapping(value = "/getUserById",method = RequestMethod.POST)
     public User getUserById(@RequestParam("id") Long id)throws Exception{
        return userMapper.getUserById(id);
     }

     @RequestMapping(value = "/getUserListByMap",method = RequestMethod.POST)
     public List<User>	getUserListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return userMapper.getUserListByMap(param);
     }

     @RequestMapping(value = "/getUserCountByMap",method = RequestMethod.POST)
     public Integer getUserCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return userMapper.getUserCountByMap(param);
     }

     @RequestMapping(value = "/addUser",method = RequestMethod.POST)
     public Integer addUser(@RequestBody User user)throws Exception{
        return userMapper.insertUser(user);
     }

     @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
     public Integer updateUser(@RequestBody User user)throws Exception{
        return userMapper.updateUser(user);
     }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public User login(@RequestParam("phone") String phone)throws Exception{
        return userMapper.login(phone);
    }

    @RequestMapping(value = "/loginAdd",method = RequestMethod.POST)
    public int loginAdd(@RequestParam("phone") String phone)throws Exception{
        return userMapper.loginAdd(phone);
    }

    @RequestMapping(value = "/allTheAttention",method = RequestMethod.POST)
    public List<User>allTheAttention(Long id) throws Exception {
         Map map = new HashMap();
         map.put("userId",id);
         List<Attention>list = attentionMapper.getAttentionListByMap(map);
         List list11 = new ArrayList();
         for (Attention attention:list){
             User user = userMapper.getUserById(attention.getTargetId());
             list11.add(user);
         }
         return list11;
    }
}
