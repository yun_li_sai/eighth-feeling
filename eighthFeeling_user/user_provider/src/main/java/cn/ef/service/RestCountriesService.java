package cn.ef.service;

import cn.ef.mapper.CountriesMapper;

import cn.ef.pojo.user.Countriesofregion;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RestCountriesService {

     @Resource
     private CountriesMapper countriesMapper;

     @RequestMapping(value = "/getCountriesById",method = RequestMethod.POST)
     public Countriesofregion getCountriesById(@RequestParam("id") Long id)throws Exception{
        return countriesMapper.getCountriesById(id);
     }

     @RequestMapping(value = "/getCountriesListByMap",method = RequestMethod.POST)
     public List<Countriesofregion>	getCountriesListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return countriesMapper.getCountriesListByMap(param);
     }

     @RequestMapping(value = "/getCountriesCountByMap",method = RequestMethod.POST)
     public Integer getCountriesCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return countriesMapper.getCountriesCountByMap(param);
     }

     @RequestMapping(value = "/addCountries",method = RequestMethod.POST)
     public Integer addCountries(@RequestBody Countriesofregion countriesofregion)throws Exception{
        return countriesMapper.insertCountries(countriesofregion);
     }

     @RequestMapping(value = "/modifyCountries",method = RequestMethod.POST)
     public Integer modifyCountries(@RequestBody Countriesofregion countriesofregion)throws Exception{
        return countriesMapper.updateCountries(countriesofregion);
     }
}
