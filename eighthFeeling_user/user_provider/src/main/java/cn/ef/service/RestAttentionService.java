package cn.ef.service;

import cn.ef.mapper.AttentionMapper;

import cn.ef.pojo.user.Attention;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class RestAttentionService {

     @Resource
     private AttentionMapper attentionMapper;

     @RequestMapping(value = "/getAttentionById",method = RequestMethod.POST)
     public Attention getAttentionById(@RequestParam("id") Long id)throws Exception{
        return attentionMapper.getAttentionById(id);
     }

     @RequestMapping(value = "/getAttentionListByMap",method = RequestMethod.POST)
     public List<Attention>	getAttentionListByMap(@RequestParam Map<String,Object> param)throws Exception{
        return attentionMapper.getAttentionListByMap(param);
     }

     @RequestMapping(value = "/getAttentionCountByMap",method = RequestMethod.POST)
     public Integer getAttentionCountByMap(@RequestParam Map<String,Object> param)throws Exception{
        return attentionMapper.getAttentionCountByMap(param);
     }

     @RequestMapping(value = "/addAttention",method = RequestMethod.POST)
     public Integer addAttention(@RequestBody Attention attention)throws Exception{
        attention.setAttentionTime(new Date());
        return attentionMapper.insertAttention(attention);
     }

     @RequestMapping(value = "/updateAttention",method = RequestMethod.POST)
     public Integer updateAttention(@RequestBody Attention attention)throws Exception{
        attention.setAttentionTime(new Date());
        return attentionMapper.updateAttention(attention);
     }
}
