package cn.ef.mapper;

import cn.ef.pojo.user.Attention;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AttentionMapper {

	public Attention getAttentionById(@Param(value = "id") Long id)throws Exception;

	public List<Attention>	getAttentionListByMap(Map<String, Object> param)throws Exception;

	public Integer getAttentionCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertAttention(Attention attention)throws Exception;

	public Integer updateAttention(Attention attention)throws Exception;

	public Integer deleteAttentionById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteAttention(Map<String, List<String>> params);

}
