package cn.ef.mapper;
import cn.ef.pojo.user.Information;

import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InformationMapper {

	public Information getInformationById(@Param(value = "id") Long id)throws Exception;

	public List<Information>	getInformationListByMap(Map<String, Object> param)throws Exception;

	public Integer getInformationCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertInformation(Information information)throws Exception;

	public Integer updateInformation(Information information)throws Exception;

	public Integer deleteInformationById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteInformation(Map<String, List<String>> params);

}
