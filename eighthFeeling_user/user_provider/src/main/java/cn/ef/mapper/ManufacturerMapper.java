package cn.ef.mapper;


import cn.ef.pojo.user.Manufacturer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ManufacturerMapper {

	public Manufacturer getManufacturerById(@Param(value = "id") Long id)throws Exception;

	public List<Manufacturer>	getManufacturerListByMap(Map<String, Object> param)throws Exception;

	public Integer getManufacturerCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertManufacturer(Manufacturer manufacturer)throws Exception;

	public Integer updateManufacturer(Manufacturer manufacturer)throws Exception;

	public Integer deleteManufacturerById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteManufacturer(Map<String, List<String>> params);

}
