package cn.ef.mapper;


import cn.ef.pojo.user.Countriesofregion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CountriesMapper {

	public Countriesofregion getCountriesById(@Param(value = "id") Long id)throws Exception;

	public List<Countriesofregion>	getCountriesListByMap(Map<String, Object> param)throws Exception;

	public Integer getCountriesCountByMap(Map<String, Object> param)throws Exception;

	public Integer insertCountries(Countriesofregion countries)throws Exception;

	public Integer updateCountries(Countriesofregion countries)throws Exception;

	public Integer deleteCountriesById(@Param(value = "id") Long id)throws Exception;

	public Integer batchDeleteCountries(Map<String, List<String>> params);

}
