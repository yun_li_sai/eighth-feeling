package cn.ef.client;

import cn.ef.fallback.CountriesClientFallBack;

import cn.ef.pojo.user.Countriesofregion;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "user-provider",  fallback = CountriesClientFallBack.class)
public interface RestCountriesClient {

    @RequestMapping(value = "/getCountriesById",method = RequestMethod.POST)
    public Countriesofregion getCountriesById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getCountriesListByMap",method = RequestMethod.POST)
    public List<Countriesofregion>	getCountriesListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getCountriesCountByMap",method = RequestMethod.POST)
    public Integer getCountriesCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addCountries",method = RequestMethod.POST)
    public Integer addCountries(@RequestBody Countriesofregion countries)throws Exception;

    @RequestMapping(value = "/modifyCountries",method = RequestMethod.POST)
    public Integer modifyCountries(@RequestBody Countriesofregion countries)throws Exception;
}
