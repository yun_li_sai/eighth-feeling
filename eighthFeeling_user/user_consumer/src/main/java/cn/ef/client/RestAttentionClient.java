package cn.ef.client;

import cn.ef.fallback.AttentionClientFallBack;

import cn.ef.pojo.user.Attention;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "user-provider",  fallback = AttentionClientFallBack.class)
public interface RestAttentionClient {

    @RequestMapping(value = "/getAttentionById",method = RequestMethod.POST)
    public Attention getAttentionById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getAttentionListByMap",method = RequestMethod.POST)
    public List<Attention>	getAttentionListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getAttentionCountByMap",method = RequestMethod.POST)
    public Integer getAttentionCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addAttention",method = RequestMethod.POST)
    public Integer addAttention(@RequestBody Attention attention)throws Exception;

    @RequestMapping(value = "/updateAttention",method = RequestMethod.POST)
    public Integer modifyAttention(@RequestBody Attention attention)throws Exception;


}
