package cn.ef.client;

import cn.ef.dto.Dto;
import cn.ef.fallback.UserClientFallBack;

import cn.ef.pojo.forum.Post;
import cn.ef.pojo.user.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "user-provider",  fallback = UserClientFallBack.class)
public interface RestUserClient {

    @RequestMapping(value = "/getUserById",method = RequestMethod.POST)
    public User getUserById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getUserListByMap",method = RequestMethod.POST)
    public List<User>	getUserListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getUserCountByMap",method = RequestMethod.POST)
    public Integer getUserCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public Integer addUser(@RequestBody User user)throws Exception;

    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public Integer updateUser(@RequestBody User user)throws Exception;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public User login(@RequestParam("phone") String phone)throws Exception;

    @RequestMapping(value = "/loginAdd",method = RequestMethod.POST)
    public int loginAdd(@RequestParam("phone") String phone)throws Exception;

    @RequestMapping(value = "/allTheAttention",method = RequestMethod.POST)
    public List<User>allTheAttention(@RequestParam("id")Long id);

    @RequestMapping("/newPostByAttention")
    public List<Post>newPostByAttention(List<Long> list);
}
