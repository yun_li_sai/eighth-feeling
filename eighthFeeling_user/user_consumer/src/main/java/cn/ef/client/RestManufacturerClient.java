package cn.ef.client;

import cn.ef.fallback.ManufacturerClientFallBack;

import cn.ef.pojo.user.Manufacturer;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "user-provider",  fallback = ManufacturerClientFallBack.class)
public interface RestManufacturerClient {

    @RequestMapping(value = "/getManufacturerById",method = RequestMethod.POST)
    public Manufacturer getManufacturerById(@RequestParam("id") Long id)throws Exception;

    @RequestMapping(value = "/getManufacturerListByMap",method = RequestMethod.POST)
    public List<Manufacturer>	getManufacturerListByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/getManufacturerCountByMap",method = RequestMethod.POST)
    public Integer getManufacturerCountByMap(@RequestParam Map<String, Object> param)throws Exception;

    @RequestMapping(value = "/addManufacturer",method = RequestMethod.POST)
    public Integer addManufacturer(@RequestBody Manufacturer manufacturer)throws Exception;

    @RequestMapping(value = "/modifyManufacturer",method = RequestMethod.POST)
    public Integer modifyManufacturer(@RequestBody Manufacturer manufacturer)throws Exception;
}
