package cn.ef.controller;

import cn.ef.client.RestAttentionClient;
import cn.ef.client.RestUserClient;

import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;

import cn.ef.pojo.forum.Post;
import cn.ef.pojo.user.Attention;
import cn.ef.pojo.user.User;
import cn.ef.util.MD5;
import cn.ef.util.RedisUtil;
import cn.ef.util.UserAgentUtil;

import com.cloopen.rest.sdk.BodyType;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Resource
    private RestUserClient restUserClient;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private RestAttentionClient attentionClient;


    private SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");


    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public Dto login(String phone, HttpServletRequest request)throws Exception {
        User loginUser=restUserClient.login(phone);
        if(loginUser==null){
            System.out.println("null");
            int count=restUserClient.loginAdd(phone);
            System.out.println(count);
            loginUser=restUserClient.login(phone);
            System.out.println(loginUser.getPhone());
        }

        //生成token
        //客户端标识-USERCODE-USERID-CREATIONDATE-RANDOM[6位]
        String userAgent=request.getHeader("user-agent");
        boolean isMobile= UserAgentUtil.CheckAgent(userAgent);
        String token=new StringBuilder(isMobile?"MOBILE":"PC").append("-")
                .append(MD5.getMd5(loginUser.getPhone(),32))
                .append("-")
                .append(loginUser.getId())
                .append("-")
                .append(sdf.format(new Date()))
                .append("-")
                .append(MD5.getMd5(userAgent,6))
                .toString();
        //把token放到redis 中保存
        redisUtil.set("token:"+token, loginUser );
        if(!isMobile){
            redisUtil.expire("token:"+token,60*60);
        }else{
            redisUtil.expire("token:"+token,7*24*60*60);
        }
        //把token 发给客户端
        return DtoUtil.returnDataSuccess(token);

    }


    @RequestMapping(value = "/sendMessage",method = RequestMethod.POST)
    public String sendMessage(String phone) {
        //生产环境请求地址：app.cloopen.com
        String serverIp = "app.cloopen.com";
        //请求端口
        String serverPort = "8883";
        //主账号,登陆云通讯网站后,可在控制台首页看到开发者主账号ACCOUNT SID和主账号令牌AUTH TOKEN
        String accountSId = "8aaf07086f6955dd016f79e632610870";
        String accountToken = "0e4cc3a3303a47c3aa1990b9855a9f0f";
        //请使用管理控制台中已创建应用的APPID
        String appId = "8aaf07086f6955dd016f79e632c40877";
        CCPRestSmsSDK sdk = new CCPRestSmsSDK();
        sdk.init(serverIp, serverPort);
        sdk.setAccount(accountSId, accountToken);
        sdk.setAppId(appId);
        sdk.setBodyType(BodyType.Type_JSON);
        String to = phone;
        String templateId= "1";
        String yzm=String.valueOf(MD5.getRandomCode());
        String[] datas = {yzm,"5"};
        redisUtil.set(yzm,"sendMessage");
        redisUtil.expire(yzm,60);
        HashMap<String, Object> result = sdk.sendTemplateSMS(to,templateId,datas);
        if("000000".equals(result.get("statusCode"))){
            //正常返回输出data包体信息（map）
            HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
            Set<String> keySet = data.keySet();
            for(String key:keySet){
                Object object = data.get(key);
                System.out.println(key +" = "+object);
            }
            return null;
        }else{
            //异常返回输出错误码和错误信息
            String err="错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg");
            System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
            return  err;
        }

    }


    @RequestMapping(value = "/yanzhenma",method = RequestMethod.POST)
    public Dto yanZhen(String phone,String yzm){
        if(yzm!=null){
            if(redisUtil.haskey(phone)){
               String yanzhenma=(String) redisUtil.get(phone);
               if(yanzhenma.equals(yzm)){
                   return  DtoUtil.returnSuccess("验证成功");
               }
                return  DtoUtil.returnFail("error","400");
            }else {
                return  DtoUtil.returnFail("error","400");
            }
        }else {
            return  DtoUtil.returnFail("error","400");
        }


    }


    @RequestMapping(value = "/sendYzm",method = RequestMethod.POST)
    public Dto sendYzm(String phone) {
        String yzm=String.valueOf(MD5.getRandomCode());
        redisUtil.set(phone,yzm);
        redisUtil.expire(phone,60);
        return DtoUtil.returnSuccess("send success!");
    }



    /**
     * 获取用户信息
     * @param id
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/getUserById")
    public Dto getUserById(Long id,@RequestHeader("token")String token) throws Exception {
        boolean result = redisUtil.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            User user = restUserClient.getUserById(id);
            if (user!=null){
               return DtoUtil.returnDataSuccess(user);
            }else {
                return DtoUtil.returnFail("获取失败","2222");
            }

        }
    }

    /**
     * 修改用户信息
     */
    @RequestMapping("/modifyUser")
    public Dto modifyUser(@RequestBody Map map)throws Exception{
        String token = (String) map.get("token");
        boolean result = redisUtil.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            User user = (User) map.get("user");
            int count = restUserClient.updateUser(user);
            if (count==-1){
                return DtoUtil.returnFail("请先登录","1111");
            }
            if(count>0){
                return DtoUtil.returnSuccess();
            }else{
                return DtoUtil.returnFail("修改失败","2222");
            }
        }
    }

    /**
     * 查询全部关注的用户的基本信息
     * @param id
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/allTheAttention")
    public Dto allTheAttention(Long id,@RequestHeader("token") String token) throws Exception {
        boolean result = redisUtil.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            List<User>list = restUserClient.allTheAttention(id);
            if (list!=null&&list.size()>0){
                return DtoUtil.returnDataSuccess(list);
            }else {
                return DtoUtil.returnFail("获取失败","2222");
            }

        }

    }

    /**
     * 查看关注用户的最新动态
     * @param userId
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/newPostByAttention")
    public Dto newPostByAttention(Long userId,@RequestHeader("token")String token) throws Exception {
        boolean result = redisUtil.haskey(token);
        if(!result){
            return DtoUtil.returnFail("请先登录","1111");
        }else {
            Map map1 = new HashMap();
            map1.put("userId",userId);
            List<Attention>list1 = attentionClient.getAttentionListByMap(map1);
            List<Long> list2 = new ArrayList<Long>();
            for(Attention attention:list1){
               list2.add(attention.getTargetId());
            }
            List<Post>list = restUserClient.newPostByAttention(list2);
            if(list!=null&&list.size()>0){
                return DtoUtil.returnDataSuccess(list);
            }else {
                return DtoUtil.returnFail("获取失败","2222");
            }
        }
    }

}
