package cn.ef.controller;

import cn.ef.client.RestAttentionClient;
import cn.ef.client.RestUserClient;
import cn.ef.client.detail.RestDetailClient;

import cn.ef.client.user.RestInformationClient;

import cn.ef.dto.Dto;
import cn.ef.dto.DtoUtil;
import cn.ef.pojo.detail.Detail;
import cn.ef.pojo.user.Attention;
import cn.ef.pojo.user.Information;
import cn.ef.pojo.user.User;
import cn.ef.util.RedisUtil;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

@RestController
public class AttentionController {
    @Resource
    RestAttentionClient restAttentionClient;

    @Resource
    RestDetailClient restDetailClient;

    @Resource
    RestUserClient restUserClient;

    @Resource
    RestInformationClient informationClient;


    @Resource
    RedisUtil redisUtils;


    /**
     * 关注
     *
     * @return
     */
    @RequestMapping("/attention")
    @Transactional
    public Dto attention(Long id, Long targetId, @RequestHeader("token") String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if (!result) {
            return DtoUtil.returnFail("请先登录", "1111");
        } else {
            User user = new User();
            user.setId(id);
            user.setAttentionNumber(1);

            User user1 = new User();
            user.setId(targetId);
            user.setNumberOfFans(1);

            restUserClient.updateUser(user);
            restUserClient.updateUser(user1);

            Attention attention = new Attention();
            attention.setUserId(id);
            attention.setTargetId(targetId);

            Map map = new HashMap();
            map.put("userId", id);
            map.put("targetId", targetId);
            attention.setState(0);
            int count;
            //查看是否曾经关注过
            count = restAttentionClient.getAttentionCountByMap(map);
            if (count > 0) {
                //是修改状态
                attention.setA1(new Date().toString());
                count = restAttentionClient.modifyAttention(attention);
            } else {
                //否新增
                attention.setType(0);
                attention.setAttentionTime(new Date());
                count = restAttentionClient.addAttention(attention);
            }
            if (count > 0) {
                return DtoUtil.returnDataSuccess(count);
            } else {
                return DtoUtil.returnFail("关注失败", "2222");
            }
        }

    }


    /**
     * 取消关注
     *
     * @param id
     * @param token
     * @return
     * @throws Exception
     */
    @Transactional
    @RequestMapping("/cancelTheAttention")
    public Dto cancelTheAttention(Long id, Long targetId, @RequestHeader("token") String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if (!result) {
            return DtoUtil.returnFail("请先登录", "1111");
        } else {
            User user = new User();
            user.setId(id);
            user.setAttentionNumber(-1);

            User user1 = new User();
            user.setId(targetId);
            user.setNumberOfFans(-1);

            restUserClient.updateUser(user);
            restUserClient.updateUser(user1);

            Attention attention = new Attention();
            attention.setUserId(id);
            attention.setTargetId(targetId);
            attention.setState(1);
            attention.setA1(new Date().toString());
            int count = restAttentionClient.modifyAttention(attention);
            if (count > 0) {
                return DtoUtil.returnDataSuccess(count);
            } else {
                return DtoUtil.returnFail("取消失败", "2222");
            }
        }

    }

    /**
     * 全部收藏
     *
     * @param id
     * @param token
     * @return
     */
    @RequestMapping("allCollection")
    public Dto allCollection(Long id, @RequestHeader("token") String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if (!result) {
            return DtoUtil.returnFail("请先登录", "1111");
        } else {
            Map map = new HashMap();
            map.put("userId", id);
            List<Attention> list = restAttentionClient.getAttentionListByMap(map);
            List<Detail> list1 = new ArrayList<Detail>();
            if(list!=null&&list.size()>0){
                for (Attention attention : list) {
                    list1.add(restDetailClient.getDetailById(attention.getTargetId()));
                }
            }
            if (list1 != null && list1.size() > 0) {
                return DtoUtil.returnDataSuccess(list1);
            } else {
                return DtoUtil.returnFail("查询失败", "2222");
            }
        }

    }

    /**
     * 收藏
     */
    @RequestMapping("/collect")
    public Dto collect(Long id, Long targetId, @RequestHeader("token") String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if (!result) {
            return DtoUtil.returnFail("请先登录", "1111");
        } else {
            Attention attention = new Attention();
            attention.setUserId(id);
            attention.setTargetId(targetId);
            attention.setType(1);
            attention.setState(0);
            attention.setA1(new Date().toString());

            User user = new User();
            user.setId(id);
            user.setAttentionNumber(1);

            Detail detail = new Detail();
            detail.setId(targetId);
            detail.setAttention_count(1);
            int count = restAttentionClient.addAttention(attention);
            int count2 = restUserClient.updateUser(user);
            int count3 = restDetailClient.updateDetail(detail);
            if (count > 0 && count2 > 0 && count3 > 0) {
                return DtoUtil.returnSuccess();
            } else {
                return DtoUtil.returnFail("收藏失败", "2222");
            }
        }
    }


    /**
     * 取消收藏
     *
     * @param id
     * @param targetId
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping("/cancelTheCollection")
    @Transactional
    public Dto cancelTheCollection(Long attentionId, Long id, Long targetId, @RequestHeader("token") String token) throws Exception {
        boolean result = redisUtils.haskey(token);
        if (!result) {
            return DtoUtil.returnFail("请先登录", "1111");
        } else {

            User user = new User();
            user.setId(id);
            user.setAttentionNumber(-1);

            Attention attention = new Attention();
            attention.setId(attentionId);
            attention.setState(1);
            attention.setA2(new Date().toString());

            Information information = new Information();
            information.setUserId(id);
            information.setTargetId(targetId);
            int count = restUserClient.updateUser(user);
            int count1 = informationClient.addInformation(information);
            int count2 = restAttentionClient.modifyAttention(attention);
            if (count > 0 && count1 > 0 && count2 > 0) {
                return DtoUtil.returnSuccess();
            } else {
                return DtoUtil.returnFail("取消失败", "2222");
            }

        }
    }

}
