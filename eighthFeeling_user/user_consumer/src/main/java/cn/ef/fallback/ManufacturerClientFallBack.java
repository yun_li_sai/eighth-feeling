package cn.ef.fallback;

import cn.ef.client.RestManufacturerClient;
import cn.ef.pojo.user.Manufacturer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ManufacturerClientFallBack implements RestManufacturerClient {


    @Override
    public Manufacturer getManufacturerById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Manufacturer>	getManufacturerListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getManufacturerCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addManufacturer(Manufacturer manufacturer)throws Exception{
        return null;
    }

    @Override
    public Integer modifyManufacturer(Manufacturer manufacturer)throws Exception{
        return null;
    }
}
