package cn.ef.fallback;

import cn.ef.client.RestUserClient;

import cn.ef.pojo.forum.Post;
import cn.ef.pojo.user.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserClientFallBack implements RestUserClient {


    @Override
    public User getUserById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<User>	getUserListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getUserCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addUser(User user)throws Exception{
        return null;
    }

    @Override
    public Integer updateUser(User user)throws Exception{
        return null;
    }

    @Override
    public User login(String phone) throws Exception {
        return null;
    }

    @Override
    public int loginAdd(String phone) throws Exception {
        return 0;
    }

    @Override
    public List<User> allTheAttention(Long id) {
        return null;
    }

    @Override
    public List<Post> newPostByAttention(List<Long> list) {
        return null;
    }


}
