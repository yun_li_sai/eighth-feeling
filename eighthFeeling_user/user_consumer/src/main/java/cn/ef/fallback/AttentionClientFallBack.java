package cn.ef.fallback;

import cn.ef.client.RestAttentionClient;
import cn.ef.pojo.user.Attention;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Map;

@Component
public class AttentionClientFallBack implements RestAttentionClient {


    @Override
    public Attention getAttentionById(Long id)throws Exception{
        return null;
    }

    @Override
    public List<Attention>	getAttentionListByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer getAttentionCountByMap(Map<String,Object> param)throws Exception{
        return null;
    }

    @Override
    public Integer addAttention(Attention attention)throws Exception{
        return null;
    }

    @Override
    public Integer modifyAttention(Attention attention)throws Exception{
        return null;
    }
}
