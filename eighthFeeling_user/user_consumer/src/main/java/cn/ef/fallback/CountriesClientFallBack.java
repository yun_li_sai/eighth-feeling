package cn.ef.fallback;

import cn.ef.client.RestCountriesClient;

import cn.ef.pojo.user.Countriesofregion;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CountriesClientFallBack implements RestCountriesClient {


    @Override
    public Countriesofregion getCountriesById(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Countriesofregion> getCountriesListByMap(Map<String, Object> param) throws Exception {
        return null;
    }

    @Override
    public Integer getCountriesCountByMap(Map<String, Object> param) throws Exception {
        return null;
    }

    @Override
    public Integer addCountries(Countriesofregion countries) throws Exception {
        return null;
    }

    @Override
    public Integer modifyCountries(Countriesofregion countries) throws Exception {
        return null;
    }
}
